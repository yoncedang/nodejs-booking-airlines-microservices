import {IRedisRepository} from "./IRedisRepository";
import {RedisClass} from "../../Redis/Redis";

const _redis = new RedisClass();

class RedisRepository implements IRedisRepository {

    public async RedisSet(key: string, value: string): Promise<string> {

        const result = await _redis.redis.set(key, value, "EX", 60 * 60 * 3);
        return result;
    }

    public async RedisGet(key: string): Promise<string | null> {
        const result = await _redis.redis.get(key);
        return result ? result : null;
    }

    public async RedisDelete(key: string): Promise<boolean> {
        try {
            const result = await _redis.redis.del(key);
            return result === 1;
        } catch (error) {
            console.error("Error deleting key from Redis:", error);
            return false;
        }
    }
}

export {RedisRepository};