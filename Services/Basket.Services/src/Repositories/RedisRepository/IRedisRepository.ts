export interface IRedisRepository {
    RedisSet: (key: string, value: string) => Promise<string>;
    RedisGet: (key: string) => Promise<string | null>;
    RedisDelete: (key: string) => Promise<boolean>;
}