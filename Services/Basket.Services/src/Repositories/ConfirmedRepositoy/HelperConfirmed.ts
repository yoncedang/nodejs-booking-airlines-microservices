import {RedisRepository} from "../RedisRepository/RedisRepository";
import {OperationResults} from "../../OperationResults/OperationResults";
import {Confirmed} from "../../Entities/Confirmed";
import {ConsumerDTO} from "../../DTO/ConsumerDTO";
import {randomUUID, UUID} from "crypto";
import {ConfirmedRepository} from "./ConfirmedRepository";
import {ConfirmedDTO} from "../../DTO/ConfirmedDTO";


const _redis = new RedisRepository();
const _operationResults = new OperationResults<Confirmed>();
const _confirmedRepository = new ConfirmedRepository();

class HelperConfirmed {


    public async CreateConfirmedHelper(consumerDTO: ConsumerDTO): Promise<OperationResults<Confirmed>> {

        const ticketCode = randomUUID().split("-")[0].toUpperCase();

        const confirmed = new Confirmed();
        confirmed.UserId = <UUID>consumerDTO.metadata.UserId;
        confirmed.Basket = <UUID>consumerDTO.metadata.BasketId;
        confirmed.TicketCode = ticketCode;


        return _operationResults.Success(confirmed, "Success");
    }

    public async FindOneByTicketCode(ticketCode: string): Promise<OperationResults<Confirmed>> {
        const result = await _confirmedRepository.FindOneByTicketCode(ticketCode.toUpperCase());

        if (result === null) {
            return _operationResults.Fail("Confirmed not found");
        }

        return _operationResults.Success(result, "Success");
    }

    public async FindConfirmListByUser(userId: UUID): Promise<OperationResults<Confirmed[]>> {
        const result = await _confirmedRepository.FindListByUser(userId);

        if (result.length === 0) {
            return _operationResults.Fail("Confirmed not found");
        }

        return _operationResults.Success(result, "Success");
    }

    public async FindOneById(confirmId: UUID): Promise<OperationResults<Confirmed>> {
        const result = await _confirmedRepository.FindOneById(confirmId);

        if (result === null) {
            return _operationResults.Fail("Confirmed not found");
        }

        return _operationResults.Success(result, "Success");
    }
}

export {HelperConfirmed};