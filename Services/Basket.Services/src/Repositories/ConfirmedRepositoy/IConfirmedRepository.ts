import {Confirmed} from "../../Entities/Confirmed";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";


interface IConfirmedRepository {
    CreateSync(basket: Confirmed): Promise<Confirmed>;

    FindOneById(confirmId: UUID, userId: UUID): Promise<Confirmed | null>;

    FindListByUser(userId: UUID): Promise<Confirmed[]>;

    FindOneByTicketCode(ticketCode: string): Promise<Confirmed | null>;

}


export {IConfirmedRepository};