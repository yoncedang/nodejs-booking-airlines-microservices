import {DbSource} from "../../DataSource/DataSource";
import {IConfirmedRepository} from "./IConfirmedRepository";
import {Confirmed} from "../../Entities/Confirmed";
import {Repository} from "typeorm";
import {UUID} from "crypto";


class ConfirmedRepository extends DbSource implements IConfirmedRepository {

    private _repository: Repository<Confirmed>;

    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Confirmed);

    }

    public async CreateSync(basket: Confirmed): Promise<Confirmed> {
        const newBasket = this._repository.create(basket);
        return await this._repository.save(newBasket);
    }

    public async FindListByUser(userId: UUID): Promise<Confirmed[]> {
        const AllConfirmed = await this._repository.find({
            where: {UserId: userId},
            relations: ["Basket", "Basket.Customer", "Basket.Customer.Sex", "Basket.Customer.Pronouns", "Basket.Contact"]
        });
        return AllConfirmed;

    }

    public async FindOneByTicketCode(ticketCode: string): Promise<Confirmed | null> {
        const results = await this._repository.findOne({
            where: {TicketCode: ticketCode},
            relations: ["Basket", "Basket.Customer", "Basket.Customer.Sex", "Basket.Customer.Pronouns", "Basket.Contact"]
        });
        return results ? results : null;
    }

    public async FindOneById(confirmId: UUID): Promise<Confirmed | null> {
        const results = await this._repository.findOne({
            where: {Id: confirmId},
            relations: ["Basket", "Basket.Customer", "Basket.Customer.Sex", "Basket.Customer.Pronouns", "Basket.Contact"]
        });
        return results ? results : null;
    }


}

export {ConfirmedRepository};