import {OperationResults} from "../../OperationResults/OperationResults";
import {Customer} from "../../Entities/Customer";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";
import {CustomerRepository} from "./CustomerRepository";
import {CreateCustomerDTO, UpdateCustomerDTO} from "../../DTO/CustomerDTO";
import {DbSource} from "../../DataSource/DataSource";
import {Sex} from "../../Entities/Sex";
import {Pronouns} from "../../Entities/Pronouns";
import {BasketRepository} from "../BasketRepository/BasketRepository";

class HelperCustomer {
    private readonly _operationResults: OperationResults<Customer>;
    private readonly _customerRepository: CustomerRepository;
    private readonly _dataSource: DbSource;

    constructor() {
        this._operationResults = new OperationResults<Customer>();
        this._customerRepository = new CustomerRepository();
        this._dataSource = new DbSource();
    }

    public async CreateCustomer(basket: Basket, createDTO: CreateCustomerDTO, checkSexId: Sex, checkPronounsId: Pronouns): Promise<OperationResults<Customer>> {
        const customer = new Customer();
        customer.Surname = createDTO.Surname;
        customer.MiddleName = createDTO.MiddleName;
        customer.Name = createDTO.Name;
        customer.Sex = <UUID>checkSexId.Id;
        customer.Pronouns = <UUID>checkPronounsId.Id;
        customer.Basket = <UUID>basket.Id;
        customer.DateOfBirth = new Date(createDTO.DateOfBirth);

        return this._operationResults.Success(customer, "Create success!");
    }


    public async CheckSexSync(sexId: UUID): Promise<OperationResults<Sex>> {
        const _repositorySex = this._dataSource._dataSource.getRepository(Sex);
        const checkSex = await _repositorySex.findOne({where: {Id: sexId}});
        if (checkSex === null) {
            return this._operationResults.Fail(`${sexId} not found`);
        }
        return this._operationResults.Success(checkSex, "Find success!");
    }

    public async CheckPronounsSync(pronounsId: UUID): Promise<OperationResults<Pronouns>> {
        const _repositoryPronouns = this._dataSource._dataSource.getRepository(Pronouns);
        const checkPronouns = await _repositoryPronouns.findOne({where: {Id: pronounsId}});
        if (checkPronouns === null) {
            return this._operationResults.Fail(`${pronounsId} not found`);
        }
        return this._operationResults.Success(checkPronouns, "Find success!");

    }

    public async CheckCustomerIdSync(customerId: UUID): Promise<OperationResults<Customer>> {
        const checkCustomer = await this._customerRepository.FindOneSync(customerId);
        if (checkCustomer === null) {
            return this._operationResults.Fail(`${customerId} not found`);
        }
        return this._operationResults.Success(checkCustomer, "Find success!");
    }

    public async UpdateCustomerIdSync(customer: Customer, updateCustomer: UpdateCustomerDTO): Promise<OperationResults<Customer>> {
        const newCustomer = new Customer();
        customer.Sex = updateCustomer.SexId ?? customer.Sex;
        customer.Surname = updateCustomer.Surname ?? customer.Surname;
        customer.MiddleName = updateCustomer.MiddleName ?? customer.MiddleName;
        customer.Name = updateCustomer.Name ?? customer.Name;
        customer.DateOfBirth = new Date(updateCustomer.DateOfBirth) ?? customer.DateOfBirth;
        customer.UpdateAt = new Date();

        return this._operationResults.Success(customer, "Update success!");
    }

    public async DeleteCustomerByBasketId(basketId: UUID): Promise<void> {
        await this._customerRepository.DeleteCustomerByBasketId(basketId);
    }

    public async DeleteCustomerArrByBasketId(basketId: UUID): Promise<void> {
        const basketArr = await this._customerRepository.FindCustomerArrayByBasketId(basketId);

        for (const customer of basketArr) {
            await this.DeleteCustomerByBasketId(customer.Basket);
        }
    }
}

export {HelperCustomer};