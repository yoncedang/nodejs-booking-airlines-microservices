import {DbSource} from "../../DataSource/DataSource";
import {ICustomerRepository} from "./ICustomerRepository";
import {Customer} from "../../Entities/Customer";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";
import {Repository} from "typeorm";


class CustomerRepository extends DbSource implements ICustomerRepository {

    private _repository: Repository<Customer>;

    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Customer);
    }

    public async CreateSync(customer: Partial<Customer>): Promise<Customer> {
        const newCustomer = this._repository.create(customer);
        return await this._repository.save(newCustomer);
    }

    public async DeleteSync(customer: Customer): Promise<boolean> {
        const results = await this._repository.remove(customer);
        if (results === null) {
            return false;
        }
        return true;
    }

    public async FindAllSync(basketId: UUID): Promise<Customer[]> {
        return this._repository.find({where: {Basket: basketId}});
    }


    public async FindOneSync(id: UUID): Promise<Customer | null> {
        return this._repository.findOne({where: {Id: id}});
    }

    public async FindOneByBasketSync(basketId: UUID): Promise<Customer | null> {
        return this._repository.findOne({where: {Basket: basketId}});
    }

    public async DeleteCustomerByBasketId(basketId: UUID): Promise<void> {
        await this._repository.delete({Basket: basketId});
    }

    public async UpdateSync(id: UUID, customer: Partial<Customer>): Promise<Customer | null> {
        const results = await this._repository.update(id, customer);
        if (results === null) {
            return null;
        }
        return this.FindOneSync(id);
    }

    public async FindCustomerArrayByBasketId(basketId: UUID): Promise<Customer[]> {
        return this._repository.find({where: {Basket: basketId}});
    }

}

export {CustomerRepository};