import {UUID} from "crypto";
import {Customer} from "../../Entities/Customer";
import {Basket} from "../../Entities/Basket";

interface ICustomerRepository {
    FindOneSync(id: UUID): Promise<Customer | null>;

    CreateSync(customer: Customer): Promise<Customer>;

    DeleteSync(customer: Customer): Promise<boolean>;

    FindAllSync(basketId: UUID): Promise<Customer[]>;

    DeleteCustomerByBasketId(basketId: UUID): Promise<void>;



}

export {ICustomerRepository};