import {OperationResults} from "../../OperationResults/OperationResults";
import {Contact} from "../../Entities/Contact";
import {CreateContactDTO, UpdateContactDTO} from "../../DTO/ContactDTO";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";
import {ContactRepository} from "./ContactRepository";
import {Sex} from "../../Entities/Sex";
import {Pronouns} from "../../Entities/Pronouns";
import {BasketRepository} from "../BasketRepository/BasketRepository";

class HelperContact {

    private readonly _operationResults: OperationResults<Contact>;
    private readonly _contactRepository: ContactRepository;


    constructor() {
        this._operationResults = new OperationResults<Contact>();
        this._contactRepository = new ContactRepository();
    }

    public async CreateContact(basket: Basket, createDTO: CreateContactDTO): Promise<OperationResults<Contact>> {

        const contact = new Contact();
        contact.Basket = basket.Id;
        contact.Surname = createDTO.Surname;
        contact.Name = createDTO.Name;
        contact.Email = createDTO.Email;
        contact.Phone = createDTO.Phone.toString();


        return this._operationResults.Success(contact, "Success");
    }

    public async FindContactListByBasket(baskets: Basket[]): Promise<OperationResults<Contact[]>> {
        const result = await this._contactRepository.FindContactListByBasket(baskets);
        if (result.length === 0, result === null || result === undefined) {
            return this._operationResults.Fail("Contact not found");
        }
        return this._operationResults.Success(result, "Contact List!");
    }

    public async CheckContactListByBasket(contactId: UUID, basket: Basket[]): Promise<OperationResults<Contact>> {
        if (basket.length === 0 || basket === null || basket === undefined) {
            return this._operationResults.Fail("Basket not found");
        }

        const contactListFinding = await this._contactRepository.FindContactListByBasket(basket);
        if (contactListFinding.length === 0 || contactListFinding === null || contactListFinding === undefined) {
            return this._operationResults.Fail("Contact not found");
        }

        for (const contact of contactListFinding) {
            if (contact.Id === contactId) {
                return this._operationResults.Success(contact, "Contact Found");
            }
        }
        return this._operationResults.Fail("Contact not found - You don't have permission to (Delete or Update) this contact of another user");
    }

    public async UpdateContact(contactId: UUID, contact: Contact, updateDTO: UpdateContactDTO): Promise<OperationResults<Contact>> {

        contact.Surname = updateDTO.Surname ?? contact.Surname;
        contact.Name = updateDTO.Name ?? contact.Name;
        contact.Email = updateDTO.Email ?? contact.Email;
        contact.Phone = updateDTO.Phone.toString() ?? contact.Phone;

        const updateContact = await this._contactRepository.UpdateSync(contactId, contact);
        if (updateContact === null) {
            return this._operationResults.Fail("Error while updating contact");
        }

        return this._operationResults.Success(updateContact, "Contact Updated");
    }

    public async DeleteContactByBasketId(basketId: UUID): Promise<void> {
        await this._contactRepository.DeleteContactIdByBasketId(basketId);
    }
}

export {HelperContact};
