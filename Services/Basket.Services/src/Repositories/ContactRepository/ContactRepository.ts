import {IContactRepository} from "./IContactRepository";
import {Repository} from "typeorm";
import {Contact} from "../../Entities/Contact";
import {DbSource} from "../../DataSource/DataSource";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";


class ContactRepository extends DbSource implements IContactRepository {

    private _repository: Repository<Contact>;


    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Contact);

    }

    public async CreateSync(contact: Partial<Contact>): Promise<Contact> {
        const newContact = this._repository.create(contact);
        return await this._repository.save(newContact);
    }

    public async DeleteSync(id: UUID): Promise<boolean> {
        const contactFinding = await this.FindOneSync(id);
        if (contactFinding === null) {
            return false;
        }
        await this._repository.remove(contactFinding);
        return true;
    }

    public async FindAllSync(): Promise<Contact[]> {
        return await this._repository.find();
    }

    public async FindOneSync(contactID: UUID): Promise<Contact | null> {
        return await this._repository.findOne({where: {Id: contactID}});
    }

    public async FindContactListByBasketID(BasketID: UUID): Promise<Contact[] | null> {
        return await this._repository.find({where: {Basket: BasketID}});
    }

    public async UpdateSync(contactID: UUID, contact: Contact): Promise<Contact | null> {
        const updateContact = await this._repository.update(contactID, contact);
        if (updateContact.affected === 0) {
            // Không tìm thấy contactId cần update
            return null;
        }

        return await this.FindOneSync(contactID);

    }

    public async FindContactListByBasket(basket: Basket[]): Promise<Contact[]> {
        let ContactList: Contact[] = [];
        for (const basketItem of basket) {
            const contact = await this._repository.findOne({where: {Basket: basketItem.Id}});
            if (contact === undefined || contact === null) {
                continue;
            }
            ContactList.push(contact);
        }

        return ContactList;
    }

    public async DeleteContactIdByBasketId(basketId: UUID): Promise<void> {
        await this._repository.delete({Basket: basketId});
    }

    public async FindOneByBasketIdSync(basketId: UUID): Promise<Contact | null> {
        return await this._repository.findOne({where: {Basket: basketId}});
    }
}

export {ContactRepository};