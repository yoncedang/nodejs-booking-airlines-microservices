import {Contact} from "../../Entities/Contact";
import {UUID} from "crypto";
import {Basket} from "../../Entities/Basket";


interface IContactRepository {
    FindOneSync(id: UUID): Promise<Contact | null>;

    CreateSync(contact: Contact): Promise<Contact>;

    FindContactListByBasketID(userId: UUID): Promise<Contact[] | null>;


    UpdateSync(id: UUID, basket: Contact): Promise<Contact | null>;

    DeleteSync(id: UUID): Promise<boolean>;

    FindAllSync(): Promise<Contact[]>;

    FindContactListByBasket(basket: Basket[]): Promise<Contact[]>;


}

export {IContactRepository};