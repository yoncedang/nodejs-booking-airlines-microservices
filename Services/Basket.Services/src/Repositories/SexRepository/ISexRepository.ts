import {Sex} from "../../Entities/Sex";
import {OperationResults} from "../../OperationResults/OperationResults";
import {UUID} from "crypto";


interface ISexRepository {
    CreateSync(sex: Sex): Promise<OperationResults<Sex>>;

    GetOneSync(sexId: UUID): Promise<OperationResults<Sex>>;

    GetSync(): Promise<OperationResults<Sex[]>>;

    UpdateSync(sexId: UUID, sex: Sex): Promise<OperationResults<Sex>>;

    DeleteSync(sexId: UUID): Promise<OperationResults<UUID>>;
}

export {ISexRepository};