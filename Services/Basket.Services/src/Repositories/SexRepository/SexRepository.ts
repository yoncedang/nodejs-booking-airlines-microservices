import {ISexRepository} from "./ISexRepository";
import {Sex} from "../../Entities/Sex";
import {OperationResults} from "../../OperationResults/OperationResults";
import {UUID} from "crypto";
import {DbSource} from "../../DataSource/DataSource";
import {Repository} from "typeorm";

class SexRepository extends DbSource implements ISexRepository {

    private _repository: Repository<Sex>;
    private _operationResults: OperationResults<Sex>;

    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Sex);
        this._operationResults = new OperationResults<Sex>();
    }

    public async CreateSync(sex: Sex): Promise<OperationResults<Sex>> {
        const createSex = this._repository.create(sex);
        const save = await this._repository.save(createSex);

        if (save === null || save === undefined) {
            return this._operationResults.Fail("Sex not created");
        }

        return this._operationResults.Success(save, "Sex created");
    }

    public async DeleteSync(sexId: UUID): Promise<OperationResults<UUID>> {
        const result = await this._repository.delete(sexId);
        if (result.affected === 0) {
            return this._operationResults.Fail("Error deleting!");
        }

        return this._operationResults.Success(sexId, "Deleted success");
    }

    public async GetOneSync(sexId: UUID): Promise<OperationResults<Sex>> {
        const result = await this._repository.findOne({where: {Id: sexId}});
        if (result === null || result === undefined) {
            return this._operationResults.Fail("Not found!");
        }

        return this._operationResults.Success(result, "Success");
    }

    public async GetSync(): Promise<OperationResults<Sex[]>> {
        const result = await this._repository.find({where: {Status: true}});
        if (result === null || result.length === 0) {
            return this._operationResults.Fail("Not found!");
        }

        return this._operationResults.Success(result, "Success");
    }

    public async UpdateSync(sexId: UUID, sex: Sex): Promise<OperationResults<Sex>> {
        const result = await this._repository.update(sexId, sex);
        if (result.affected === 0) {
            return this._operationResults.Fail("Not found!");
        }
        const sexResult = await this.GetOneSync(sexId);

        return this._operationResults.Success(sexResult._Data, "Updated success");
    }

}

export {SexRepository};