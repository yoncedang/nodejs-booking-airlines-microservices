import {IBasketRepository} from "./IBasketRepository";
import {Basket} from "../../Entities/Basket";
import {UUID} from "crypto";
import {DbSource} from "../../DataSource/DataSource";
import {Repository} from "typeorm";
import {HelperContact} from "../ContactRepository/HelperContact";
import {HelperCustomer} from "../CustomerRepository/HelperCustomer";

class BasketRepository extends DbSource implements IBasketRepository {


    private _repository: Repository<Basket>;
    private _helperContact: HelperContact;
    private _helperCustomer: HelperCustomer;

    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Basket);
        this._helperContact = new HelperContact();
        this._helperCustomer = new HelperCustomer();
    }


    public async CreateSync(basket: Partial<Basket>): Promise<Basket> {
        const newBasket = this._repository.create(basket);

        return await this._repository.save(newBasket);

    }

    public async DeleteSync(id: UUID): Promise<boolean> {
        const BasketFinding = await this.FindOneSync(id);
        if (BasketFinding === null) {
            return false;
        }
        await this._repository.remove(BasketFinding);
        return true;
    };

    public async FindAllSync(): Promise<Basket[]> {
        return await this._repository.find({relations: ["Customer", "Contact"]});
    }

    public async FindOneSync(id: UUID): Promise<Basket | null> {
        return await this._repository.findOne({
            where: {Id: id},
            relations: ["Customer", "Customer.Sex", "Customer.Pronouns", "Contact"]
        });
    }


    public async FindBasketList(userId: UUID): Promise<Basket[]> {
        try {
            console.log("FindBasketList: ", userId);
            return await this._repository.find({
                where: {UserId: userId, isPending: true, Status: "Pending"},
                relations: ["Customer", "Customer.Sex", "Customer.Pronouns", "Contact"]
            });
        } catch (error: any) {
            console.log("Error: ", error.message);
            return [];
        }
    }

    public async DeleteBasketList(userId: UUID): Promise<void> {
        try {
            const BasketFinding = await this.FindBasketList(userId);
            console.log("BasketFinding: ", BasketFinding);
            if (BasketFinding.length === 0 || BasketFinding === null) {
                return console.log("Basket clean!");
            }
            for (const basket of BasketFinding) {

                await Promise.all([
                    this._helperCustomer.DeleteCustomerByBasketId(basket.Id),
                    this._helperContact.DeleteContactByBasketId(basket.Id)
                ]).then(async () => {
                    await this.DeleteSync(basket.Id);
                });
            }

            console.log("Basket delete clean!");
        } catch (error: any) {
            console.log("Error at BasketRepository: ", error.message);
        }
    }

    public async UpdateSync(id: UUID, basket: Basket): Promise<Basket | null> {
        const updateBasket = await this._repository.update(id, basket);
        if (updateBasket.affected === 0) {
            // Không tìm thấy BasketId cần update
            return null;
        }

        return await this.FindOneSync(id);
    }

    public async FindOneIsPending(userId: UUID): Promise<Basket | null> {
        return await this._repository.findOne({
            where: {UserId: userId, isPending: true, Status: "Pending"},
            relations: ["Customer", "Customer.Sex", "Customer.Pronouns", "Contact"]
        });
    }


    public async FindAllBasketByUser(userId: UUID): Promise<Basket[]> {
        const result = await this._repository.find({
            where: {UserId: userId, isPending: false, Status: "Success"},
            relations: ["Customer", "Customer.Sex", "Customer.Pronouns", "Contact"]
        });
        return result;
    }

    public async FindOnetoUpdateBasket(userId: UUID, basketID: UUID): Promise<Basket | null> {
        const results = await this._repository.findOne({
            where: {UserId: userId, Id: basketID},
        });
        return results;
    }

    public async DeleteBasketConsumer(basket: Basket): Promise<boolean> {
        const results = await this._repository.remove(basket);
        if (results === null) {
            return false;
        }
        return true;
    }

}

export {BasketRepository};
