import {UUID} from "crypto";


interface Airport {
    Id: UUID;
    Name: string;
    City: string;
    Country: string;
    IATA: string;
}

interface Airline {
    Id: UUID;
    Name: string;
    Country: string;
    IATA: string;
}

interface Aircraft {
    Id: UUID;
    Name: string;
    Model: string;
    TotalSeats: number;
    Active: boolean;
}

interface Flight {
    Flight: {
        Id: UUID;
        FlightCode: string;
        DepartureTime: string | Date;
        ArrivalTime: string | Date;
        AvailableSeats: number;
        Price: number;
        Status: boolean;
        FlightDuration: string | number | Date;
        DepartureAirport: Airport;
        ArrivalAirport: Airport;
        Airline: Airline;
        Aircraft: Aircraft;
    };
}

export {Airport, Airline, Aircraft, Flight};
