import {Basket} from "../../Entities/Basket";
import {UUID} from "crypto";

interface IBasketRepository {
    FindOneSync(id: UUID): Promise<Basket | null>;

    CreateSync(basket: Basket): Promise<Basket>;

    FindBasketList(userId: UUID): Promise<Basket[]>;

    DeleteBasketList(userId: UUID): Promise<void>;

    FindOneIsPending(userId: UUID): Promise<Basket | null>;

    FindOnetoUpdateBasket(userId: UUID, basketID: UUID): Promise<Basket | null>;

    UpdateSync(id: UUID, basket: Basket): Promise<Basket | null>;

    DeleteSync(id: UUID): Promise<boolean>;

    FindAllSync(): Promise<Basket[]>;

    FindAllBasketByUser(userId: UUID): Promise<Basket[]>;

    DeleteBasketConsumer(basket: Basket): Promise<boolean>;

    //
    // FindByFlightCode(BookingCode: string): Promise<Basket | null>;
}

export {IBasketRepository};