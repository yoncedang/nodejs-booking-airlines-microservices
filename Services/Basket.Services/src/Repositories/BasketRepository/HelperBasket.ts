import {RedisRepository} from "../RedisRepository/RedisRepository";

import {Basket} from "../../Entities/Basket";
import {CreateBasketDTO} from "../../DTO/BasketDTO";
import {Flight} from "./InterfaceType";
import {UUID, randomUUID} from "crypto";
import {OperationResults} from "../../OperationResults/OperationResults";
import {BasketRepository} from "./BasketRepository";


class HelperBasket {

    private readonly _redisRepository: RedisRepository;
    private readonly _operationResults: OperationResults<Basket>;
    private readonly _basketRepository: BasketRepository;


    constructor() {
        this._redisRepository = new RedisRepository();
        this._operationResults = new OperationResults<Basket>();
        this._basketRepository = new BasketRepository();

    }

    public async CreateBasketHelper(userId: UUID, createDTO: CreateBasketDTO): Promise<OperationResults<Basket>> {

        if (createDTO.FlightBackId && createDTO.FlightBackId === createDTO.FlightComeId) {
            return this._operationResults.Fail("Flight come and back cannot be the same");
        }

        const [FlightCome, FlightBack] = await Promise.all([
            this._redisRepository.RedisGet(createDTO.FlightComeId),
            createDTO.FlightBackId ? this._redisRepository.RedisGet(createDTO.FlightBackId) : null
        ]);

        if (FlightCome == null) {
            return this._operationResults.Fail("Flight come not found");
        }

        if (FlightBack == null && createDTO.FlightBackId) {
            return this._operationResults.Fail("Flight back not found");
        }

        const dataFlightCome: Flight = JSON.parse(FlightCome);
        const dataFlightBack: Flight = FlightBack ? JSON.parse(FlightBack) : null;


        const basket = new Basket();
        basket.UserId = userId;
        basket.FlightComeId = createDTO.FlightComeId;
        basket.FlightBackId = createDTO.FlightBackId ?? null;
        basket.AmountPassenger = createDTO.AmountPassenger;
        basket.AmountTicket = (createDTO.FlightBackId && dataFlightBack.Flight.Price !== null) ? (createDTO.AmountPassenger * 2) : (createDTO.AmountPassenger * 1);
        basket.Price = (dataFlightCome.Flight.Price * createDTO.AmountPassenger) + (createDTO.FlightBackId && dataFlightBack.Flight.Price !== null ? (dataFlightBack.Flight.Price * createDTO.AmountPassenger) : 0);
        basket.AirlinesCome = dataFlightCome.Flight.Airline.IATA;
        basket.AirlinesBack = dataFlightBack?.Flight.Airline.IATA ?? null;


        return this._operationResults.Success(basket, "Success");
    }

    public async UpdateBasketHelper(basketId: UUID, basket: Basket): Promise<OperationResults<Basket>> {
        basket.Status = "Success";
        basket.isPending = false;

        const result = await this._basketRepository.UpdateSync(basketId, basket);

        if (result === null) {
            return this._operationResults.Fail("Error while updating basket");
        }

        return this._operationResults.Success(result, "Success");

    }
}

export {HelperBasket};

//