import {Pronouns} from "../../Entities/Pronouns";
import {UUID} from "crypto";
import {OperationResults} from "../../OperationResults/OperationResults";

interface IPronounsRepository {

    CreatePronouns(pronouns: Pronouns): Promise<OperationResults<Pronouns>>;

    GetPronouns(): Promise<OperationResults<Pronouns[]>>;

    GetOnePronouns(id: UUID): Promise<OperationResults<Pronouns>>;

    DeletePronouns(id: UUID): Promise<OperationResults<UUID>>;

    UpdatePronouns(id: UUID, pronouns: Pronouns): Promise<OperationResults<Pronouns>>;
}

export {IPronounsRepository};