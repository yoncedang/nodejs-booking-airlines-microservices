import {IPronounsRepository} from "./IPronounsRepository";
import {UUID} from "crypto";
import {Pronouns} from "../../Entities/Pronouns";
import {DbSource} from "../../DataSource/DataSource";
import {Repository} from "typeorm";
import {OperationResults} from "../../OperationResults/OperationResults";

class PronounsRepository extends DbSource implements IPronounsRepository {

    private _repository: Repository<Pronouns>;
    private _operationResults: OperationResults<Pronouns>;

    constructor() {
        super();
        this._repository = this._dataSource.getRepository(Pronouns);
        this._operationResults = new OperationResults<Pronouns>();
    }

    public async DeletePronouns(id: UUID): Promise<OperationResults<UUID>> {
        const result = await this._repository.delete(id);
        if (result.affected === 0) {
            return this._operationResults.Fail("Pronouns not found");
        }

        return this._operationResults.Success(id, "Pronouns deleted");
    }

    public async GetOnePronouns(id: UUID): Promise<OperationResults<Pronouns>> {
        const result = await this._repository.findOne({where: {Id: id}});
        if (result === null || result === undefined) {
            return this._operationResults.Fail("Pronouns not found");
        }
        return this._operationResults.Success(result, "Pronouns found");
    }

    public async GetPronouns(): Promise<OperationResults<Pronouns[]>> {
        const result = await this._repository.find();
        if (result === null || result === undefined) {
            return this._operationResults.Fail("Pronouns not found");
        }
        return this._operationResults.Success(result, "Pronouns found");
    }

    public async UpdatePronouns(id: UUID, pronouns: Pronouns): Promise<OperationResults<Pronouns>> {
        const result = await this._repository.update(id, pronouns);
        if (result.affected === 0) {
            return this._operationResults.Fail("Pronouns not found");
        }
        return this._operationResults.Success(await this.GetOnePronouns(id).then(x => x._Data), "Pronouns updated");
    }

    public async CreatePronouns(pronouns: Pronouns): Promise<OperationResults<Pronouns>> {
        const createPronouns = this._repository.create(pronouns);
        const result = await this._repository.save(createPronouns);

        if (result === null || result === undefined) {
            return this._operationResults.Fail("Pronouns not created");
        }
        return this._operationResults.Success(result, "Pronouns created");
    }

}

export {PronounsRepository};