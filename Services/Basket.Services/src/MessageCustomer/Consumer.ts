import {KafkaConfig} from "./KafkaConfig";
import {RedisClass} from "../Redis/Redis";
import {ConsumerTOPIC} from "../MessageCustomer/TOPIC/ConsumerTOPIC";
import {ProducerTOPIC} from "../MessageCustomer/TOPIC/ProducerTOPIC";
import {Kafka, Consumer, Producer} from "kafkajs";
import {RedisRepository} from "../Repositories/RedisRepository/RedisRepository";
import {ConsumerDTO} from "../DTO/ConsumerDTO";
import {ConfirmedRepository} from "../Repositories/ConfirmedRepositoy/ConfirmedRepository";
import {HelperConfirmed} from "../Repositories/ConfirmedRepositoy/HelperConfirmed";
import {BasketRepository} from "../Repositories/BasketRepository/BasketRepository";
import {HelperBasket} from "../Repositories/BasketRepository/HelperBasket";
import {HelperContact} from "../Repositories/ContactRepository/HelperContact";
import {HelperCustomer} from "../Repositories/CustomerRepository/HelperCustomer";

class KafkaConsumer extends KafkaConfig {
    private Redis: RedisRepository;
    private ConsumerTOPIC: ConsumerTOPIC;
    private ProducerTOPIC: ProducerTOPIC;
    private HelperBasket: HelperBasket;
    private HelperContact: HelperContact;
    private HelperCustomer: HelperCustomer;
    private BasketCreateCode: Consumer;
    private PaymentCancelBasket: Consumer;
    private ConfirmedRepository: ConfirmedRepository;
    private HelperConfirmed: HelperConfirmed;
    private BasketRepository: BasketRepository;

    constructor() {
        super();
        this.Redis = new RedisRepository;
        this.ConfirmedRepository = new ConfirmedRepository;
        this.HelperBasket = new HelperBasket;
        this.HelperContact = new HelperContact;
        this.HelperCustomer = new HelperCustomer;
        this.BasketRepository = new BasketRepository;
        this.HelperConfirmed = new HelperConfirmed();
        this.ConsumerTOPIC = new ConsumerTOPIC;
        this.ProducerTOPIC = new ProducerTOPIC;
        this.BasketCreateCode = this.kafka.consumer({
            groupId: this.ConsumerTOPIC.BasketCreateCode,
            allowAutoTopicCreation: true,
        });
        this.PaymentCancelBasket = this.kafka.consumer({
            groupId: this.ConsumerTOPIC.PaymentCancelBasket,
            allowAutoTopicCreation: true,
        });

        // this.connect();
        // this.subscribe();
        //
        this.connect();
        this.subscribe();
    }

    private async connect(): Promise<void> {
        console.log("Connect to Kafka");
        await Promise.all([
            this.BasketCreateCode.connect(),
            this.PaymentCancelBasket.connect(),
        ]);
    }

    private async subscribe(): Promise<void> {
        // Subscribe to Kafka - Subscribe đồng thời tới tất cả các topic
        console.log("Subscribe to Kafka");
        await Promise.all([
            this.BasketCreateCode.subscribe({
                // Không đọc lại các message cũ - Don't read old messages again => fromBeginning: false
                topic: this.ConsumerTOPIC.BasketCreateCode, fromBeginning: false
            }),
            this.PaymentCancelBasket.subscribe({
                // Không đọc lại các message cũ - Don't read old messages again => fromBeginning: false
                topic: this.ConsumerTOPIC.PaymentCancelBasket, fromBeginning: false
            }),
        ]);
    }


    public async basketCreateCode(): Promise<void> {

        await this.BasketCreateCode.run({
            eachMessage: async ({topic, partition, message}): Promise<void> => {
                // Get data from Kafka Producer- Lấy dữ liệu từ Kafka
                const data = {
                    topic,
                    partition,
                    key: message.key?.toString(),
                    value: message.value?.toString(),
                };
                console.log("Key: ", data.key);

                // Check key in Redis - Kiểm tra key trong Redis
                const RedisCheckingKey = await this.Redis.RedisGet(data.key || "");
                // Chuyển dữ liệu từ string sang JSON
                const value: ConsumerDTO = JSON.parse(data.value || "");
                console.log("Value: ", value);

                if (RedisCheckingKey) {
                    console.log("Key is exists in Redis");
                    const helper = await this.HelperConfirmed.CreateConfirmedHelper(value);
                    if (helper._IsSuccess === false) {
                        console.log("Error: ", helper._Message);
                    }

                    const result = await this.ConfirmedRepository.CreateSync(helper._Data);
                    if (result === null) {
                        return console.log("Error while creating confirmed");
                    }
                    const basket = await this.BasketRepository.FindOnetoUpdateBasket(value.metadata.UserId, value.metadata.BasketId);
                    if (basket !== null) {
                        const results = await this.HelperBasket.UpdateBasketHelper(value.metadata.BasketId, basket);

                        if (results._IsSuccess === false) {
                            return console.log("Error: ", results._Message);
                        }

                        console.log("Basket Updated success: ", results._Data);
                        const deleteKey = await this.Redis.RedisDelete(data.key || "");
                        if (deleteKey === false) {
                            return console.log("Error while deleting key in Redis");
                        } else {
                            console.log("Key is deleted in Redis");
                            // Commit
                            await this.BasketCreateCode.commitOffsets([
                                {topic, partition, offset: (parseInt(message.offset) + 1).toString()},
                            ]);
                        }
                    } else {
                        console.log("Error while updating basket");
                    }


                } else {
                    console.log("Key is not exists in Redis");
                    // Commit
                    await this.BasketCreateCode.commitOffsets([
                        {topic, partition, offset: (parseInt(message.offset) + 1).toString()},
                    ]);
                }

            },
        });
    }

    public async paymentCancelBasket(): Promise<void> {

        await this.PaymentCancelBasket.run({
            eachMessage: async ({topic, partition, message}): Promise<void> => {
                // Get data from Kafka Producer- Lấy dữ liệu từ Kafka
                const data = {
                    topic,
                    partition,
                    key: message.key?.toString(),
                    value: message.value?.toString(),
                };
                console.log("Key: ", data.key);

                // Check key in Redis - Kiểm tra key trong Redis
                const RedisCheckingKey = await this.Redis.RedisGet(data.key || "");
                // Chuyển dữ liệu từ string sang JSON
                const value: ConsumerDTO = JSON.parse(data.value || "");
                console.log("Value: ", value);

                if (RedisCheckingKey) {
                    console.log("Key is exists in Redis");
                    await Promise.all([
                        this.HelperContact.DeleteContactByBasketId(value.metadata.BasketId),
                        this.HelperCustomer.DeleteCustomerArrByBasketId(value.metadata.BasketId),
                    ]);
                    const dataBasket = await this.BasketRepository.DeleteSync(value.metadata.BasketId);
                    if (dataBasket === false) {
                        await this.BasketCreateCode.commitOffsets([
                            {topic, partition, offset: (parseInt(message.offset) + 1).toString()},
                        ]);
                        return console.log("Error while deleting basket");
                    }
                    const deleteKey = await this.Redis.RedisDelete(data.key || "");
                    if (deleteKey === false) {
                        return console.log("Error while deleting key in Redis");
                    }
                    console.log("Key is deleted in Redis");
                    // Commit
                    await this.BasketCreateCode.commitOffsets([
                        {topic, partition, offset: (parseInt(message.offset) + 1).toString()},
                    ]);

                    console.log("Basket Deleted success");
                } else {
                    console.log("Key is not exists in Redis");
                    // Commit
                    await this.BasketCreateCode.commitOffsets([
                        {topic, partition, offset: (parseInt(message.offset) + 1).toString()},
                    ]);
                }

            },
        });
    }
}

export {KafkaConsumer};
