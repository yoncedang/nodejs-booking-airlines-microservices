import {KafkaConfig} from "./KafkaConfig";
import {Producer} from "kafkajs";
import {RedisRepository} from "../Repositories/RedisRepository/RedisRepository";

class KafkaProducer extends KafkaConfig {

    private producer: Producer;
    private _redisRepository: RedisRepository;

    constructor() {
        super();
        this.producer = this.kafka.producer({allowAutoTopicCreation: true});
        this._redisRepository = new RedisRepository();
        this.connect();
    }

    private async connect(): Promise<void> {
        await this.producer.connect();
    }

    public async sendMessage(topic: string, key: string, message: any): Promise<void> {
        try {
            await this._redisRepository.RedisSet(key, JSON.stringify(message));

            await this.producer.send({
                topic: topic,
                messages: [{
                    key: key,
                    value: JSON.stringify(message)
                }]
            });


        } catch (error: any) {
            console.log("Error", error.message);
        }
    }
}

export {
    KafkaProducer
};