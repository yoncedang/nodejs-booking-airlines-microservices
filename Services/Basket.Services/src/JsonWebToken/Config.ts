import jwt, {JwtPayload, JwtHeader, VerifyOptions} from "jsonwebtoken";
import {JWT_Configuration} from "../Config/Config";


class JWTConfig {

    private JWT_Configuration: any;
    private jwt = jwt;

    constructor() {
        this.JWT_Configuration = JWT_Configuration;
        this.jwt = jwt;
    }

    public async VerificationToken(token: string, callBack: (err: any, value: any) => Promise<any>): Promise<any> {

        return this.jwt.verify(token, this.JWT_Configuration.JWT_SECRET, {
            issuer: this.JWT_Configuration.JWT_ISSUER,
            audience: this.JWT_Configuration.JWT_AUDIENCE,
        }, callBack);
    }

}

export {JWTConfig};