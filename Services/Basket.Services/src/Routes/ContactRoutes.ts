import express, {Request, Response, Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";

import {ContactController} from "../Controllers/ContactController";

class ContactRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _contactController: ContactController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();

        this._contactController = new ContactController();
        this.AddContact();
        this.AllContactByUser();
        this.GetContactById();
        this.UpdateContact();
        this.DeleteContact();

    }

    private async AddContact(): Promise<void> {
        this.Router.post("/contact/add", this._middleware.VerificationTokenOrAnonymous, this._contactController.Create);
    }

    private async AllContactByUser(): Promise<void> {
        this.Router.get("/contact/all/by-user", this._middleware.VerificationAuthenticationUser, this._contactController.AllContactByUser);
    }

    private async GetContactById(): Promise<void> {
        this.Router.get("/contact/get/:id", this._middleware.TokenAuthentication, this._contactController.GetContactById);
    }

    private async UpdateContact(): Promise<void> {
        this.Router.put("/contact/update/:id", this._middleware.VerificationAuthenticationUser, this._contactController.UpdateContactById);
    }

    private async DeleteContact(): Promise<void> {
        this.Router.delete("/contact/delete/:id", this._middleware.VerificationAuthenticationUser, this._contactController.DeleteContactById);
    }

}

export {ContactRoutes};