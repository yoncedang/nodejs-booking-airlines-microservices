import express, {Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";

import {PronounsController} from "../Controllers/PronounsController";

class PronounsRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _pronounsController: PronounsController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();
        this._pronounsController = new PronounsController();
        this.PronounsCreate();
        this.PronounsDelete();
        this.GetOnePronouns();
        this.GetPronouns();
        this.UpdatePronouns();

    }

    private PronounsCreate(): void {
        this.Router.post("/pronoun/add", this._middleware.VerificationAuthenticationManager, this._pronounsController.CreatePronouns);
    }

    private PronounsDelete(): void {
        this.Router.delete("/pronoun/delete/:id", this._middleware.VerificationAuthenticationManager, this._pronounsController.DeletePronouns);
    }

    private GetOnePronouns(): void {
        this.Router.get("/pronoun/get/:id", this._middleware.VerificationTokenOrAnonymous, this._pronounsController.GetOnePronouns);
    }

    private GetPronouns(): void {
        this.Router.get("/pronoun/all", this._middleware.VerificationTokenOrAnonymous, this._pronounsController.GetPronouns);
    }

    private UpdatePronouns(): void {
        this.Router.put("/pronoun/update/:id", this._middleware.VerificationAuthenticationManager, this._pronounsController.UpdatePronouns);
    }


}

export {PronounsRoutes};