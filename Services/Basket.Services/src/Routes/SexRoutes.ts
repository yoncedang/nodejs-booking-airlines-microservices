import {SexController} from "../Controllers/SexController";
import express, {Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";


class SexRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _sexController: SexController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();
        this._sexController = new SexController();
        this.SexCreate();
        this.SexDelete();
        this.GetOneSex();
        this.GetSex();
        this.UpdateSex();

    }

    private SexCreate(): void {
        this.Router.post("/gender/add", this._middleware.VerificationAuthenticationManager, this._sexController.CreateSex);
    }

    private SexDelete(): void {
        this.Router.delete("/gender/delete/:id", this._middleware.VerificationAuthenticationManager, this._sexController.DeleteSex);
    }

    private GetOneSex(): void {
        this.Router.get("/gender/get/:id", this._middleware.VerificationTokenOrAnonymous, this._sexController.GetOneSex);
    }

    private GetSex(): void {
        this.Router.get("/gender/all", this._middleware.VerificationTokenOrAnonymous, this._sexController.GetAllSex);
    }

    private UpdateSex(): void {
        this.Router.put("/gender/update/:id", this._middleware.VerificationAuthenticationManager, this._sexController.UpdateSex);
    }


}

export {SexRoutes};