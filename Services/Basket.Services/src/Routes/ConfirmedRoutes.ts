import express, {Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";

import {ConfirmedController} from "../Controllers/ConfirmedController";

class ConfirmedRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _confirmedController: ConfirmedController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();

        this._confirmedController = new ConfirmedController();
        this.ConfirmedGetByCode();
        this.ConfirmedGetList();
        this.ConfirmedGetOneById();

    }

    private ConfirmedGetByCode(): void {
        this.Router.get("/confirmation/ticket/:code", this._confirmedController.GetConfrimedByCode);
    }

    private ConfirmedGetList(): void {
        this.Router.get("/confirmation/ticket/success/by-user", this._middleware.VerificationAuthenticationUser, this._confirmedController.FindListConfirmed);
    }

    private ConfirmedGetOneById(): void {
        this.Router.get("/confirmation/ticket/confirmed/:id", this._middleware.VerificationAuthenticationUser, this._confirmedController.FindOneById);
    }

}

export {ConfirmedRoutes};