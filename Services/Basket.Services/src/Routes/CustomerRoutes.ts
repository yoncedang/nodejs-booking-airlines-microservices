import express, {Request, Response, Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";

import {CustomerController} from "../Controllers/CustomerController";

class CustomerRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _customerController: CustomerController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();

        this._customerController = new CustomerController();
        this.AddCustomer();
        this.GetAllCustomerBasketId();
        this.UpdateCustomer();
        this.GetCustomerById();

    }

    private AddCustomer(): void {
        this.Router.post("/customer/add", this._middleware.VerificationTokenOrAnonymous, this._customerController.Create);
    }

    private GetAllCustomerBasketId(): void {
        this.Router.get("/customer/all/by-basket/:id", this._middleware.VerificationAuthenticationUser, this._customerController.GetAllCustomerOfBasketSuccess);
    }

    private UpdateCustomer(): void {
        this.Router.put("/customer/update/:id", this._middleware.VerificationAuthenticationManager, this._customerController.UpdateCustomerIdByFlightMananger);
    }

    private GetCustomerById(): void {
        this.Router.get("/customer/get/:id", this._middleware.VerificationAuthenticationUserAndManager, this._customerController.GetCustomerById);
    }

}

export {CustomerRoutes};