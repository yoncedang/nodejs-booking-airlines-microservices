import express, {Router} from "express";
import {Middleware} from "../MiddleWare/Middleware";

import {BasketController} from "../Controllers/BasketController";

class BasketRoutes {
    public Router: express.Router;

    private readonly _middleware: Middleware;
    private readonly _basketController: BasketController;

    constructor() {
        this.Router = Router();
        this._middleware = new Middleware();

        this._basketController = new BasketController();
        this.BasketBooking();
        this.BasketGetAll();
        this.BasketById();
        this.BasketCurrent();

    }

    private BasketBooking(): void {
        this.Router.post("/basket/add", this._middleware.VerifyToken, this._basketController.Create);
    }

    private BasketGetAll(): void {
        this.Router.get("/basket/all/by-user", this._middleware.VerificationAuthenticationUser, this._basketController.GetAllBasketByUserId);
    }

    private BasketById(): void {
        this.Router.get("/basket/get/:id", this._middleware.VerificationAuthenticationUser, this._basketController.GetBasketById);
    }


    private BasketCurrent(): void {
        this.Router.get("/basket/current-basket", this._middleware.VerificationTokenOrAnonymous, this._basketController.CurrentBasket);
    }
}

export {BasketRoutes};