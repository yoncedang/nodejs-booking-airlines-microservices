import {UUID} from "crypto";

interface ConsumerDTO {
    paymentStatus: string;
    metadata: Metadata;
}

interface Metadata {
    AmountPassenger: number;
    AmountTicket: number;
    BasketId: UUID;
    UserId: UUID;
    IsPending: boolean;
    Status: string;
    Price: number;
    FlightComeId: UUID | string;
    FlightBackId: UUID | string;
}

export {ConsumerDTO};