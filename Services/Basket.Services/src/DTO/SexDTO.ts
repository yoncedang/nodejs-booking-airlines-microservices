import {IsNotEmpty, IsEmail, Length, IsEmpty, IsUUID, IsNumber, Min, Max, IsString} from "class-validator";
import {UUID} from "crypto";


class CreateSexDTO {

    @IsNotEmpty({message: "Name is required"})
    @IsString({message: "Name must be a string"})
    @Length(3, 50, {message: "Name must be between 3 and 50 characters"})
    readonly Name: string;
}


export {CreateSexDTO};