import {IsNotEmpty, IsEmail, Length, IsEmpty, IsUUID, IsNumber, Min, Max, IsOptional} from "class-validator";
import {UUID} from "crypto";


class ConfirmedDTO {
    @IsNotEmpty({message: "TicketCode is required"})
    @Length(8, 8, {message: "TicketCode must be 8 characters"})
    readonly TicketCode: string;
}

export {ConfirmedDTO};