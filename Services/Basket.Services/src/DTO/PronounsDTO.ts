import {IsNotEmpty, IsEmail, Length, IsEmpty, IsUUID, IsNumber, Min, Max, IsString, IsOptional} from "class-validator";
import {UUID} from "crypto";


class UpdatePronounsDTO {

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 30, {message: "Pronoun must be between 2 and 30 characters"})
    readonly Pronoun: string;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 7, {message: "Abbreviation must be between 2 and 7 characters"})
    readonly Abbreviation: string;

}

class CreatePronounsDTO {
    @IsString({message: "Invalid string"})
    @Length(2, 30, {message: "Pronoun must be between 2 and 30 characters"})
    @IsNotEmpty({message: "Pronoun is required"})
    readonly Pronoun: string;

    @IsNotEmpty({message: "Abbreviation is required"})
    @IsString({message: "Invalid string"})
    @Length(2, 7, {message: "Abbreviation must be between 2 and 7 characters"})
    readonly Abbreviation: string;
}


export {UpdatePronounsDTO, CreatePronounsDTO};