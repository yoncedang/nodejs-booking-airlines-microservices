import {IsNotEmpty, IsEmail, Length, IsEmpty, IsUUID, IsNumber, Min, Max, IsOptional} from "class-validator";
import {UUID} from "crypto";


class CreateBasketDTO {
    @IsNotEmpty({message: "FlightComeID is required"})
    @IsUUID("4", {message: "Invalid UUID"})
    readonly FlightComeId: UUID;

    @IsOptional()
    @IsUUID("4", {message: "Invalid UUID"})
    readonly FlightBackId: UUID;

    @IsNotEmpty({message: "AmountPassenger is required"})
    @IsNumber({}, {message: "Invalid number"})
    @Min(1, {message: "Amount must be at least 1"})
    @Max(350, {message: "Amount must be at most 350"})
    readonly AmountPassenger: number;
}


export {CreateBasketDTO};