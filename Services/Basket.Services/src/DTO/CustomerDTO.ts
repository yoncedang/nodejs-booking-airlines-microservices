import {
    IsNotEmpty,
    IsEmail,
    Length,
    IsEmpty,
    IsUUID,
    IsNumber,
    Min,
    Max,
    IsString,
    IsPhoneNumber, IsDate, IsOptional
} from "class-validator";
import {UUID} from "crypto";

class CreateCustomerDTO {

    @IsNotEmpty({message: "PronounsId is required"})
    @IsUUID("4", {message: "Invalid UUID"})
    readonly PronounsId: UUID;

    @IsNotEmpty({message: "SexId is required"})
    @IsUUID("4", {message: "Invalid UUID"})
    readonly SexId: UUID;


    @IsNotEmpty({message: "Surname is required"})
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "Surname must be between 2 and 50 characters"})
    readonly Surname: string;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "MiddleName must be between 2 and 50 characters"})
    readonly MiddleName: string;

    @IsNotEmpty({message: "Name is required"})
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "Name must be between 2 and 50 characters"})
    readonly Name: string;

    @IsNotEmpty({message: "DateOfBirth is required"})
    @IsDate({message: "Invalid date"})
    readonly DateOfBirth: Date;

}

class UpdateCustomerDTO {

    @IsOptional()
    @IsUUID("4", {message: "Invalid UUID"})
    readonly SexId: UUID;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "Surname must be between 2 and 50 characters"})
    readonly Surname: string;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "MiddleName must be between 2 and 50 characters"})
    readonly MiddleName: string;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 25, {message: "Name must be between 2 and 50 characters"})
    readonly Name: string;

    @IsOptional()
    @IsDate({message: "Invalid date"})
    readonly DateOfBirth: Date;
}

export {CreateCustomerDTO, UpdateCustomerDTO};