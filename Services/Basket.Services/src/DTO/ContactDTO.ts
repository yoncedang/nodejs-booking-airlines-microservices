import {
    IsNotEmpty,
    IsEmail,
    Length,
    IsEmpty,
    IsUUID,
    IsNumber,
    Min,
    Max,
    IsString,
    IsPhoneNumber, IsOptional
} from "class-validator";
import {UUID} from "crypto";


class CreateContactDTO {

    @IsNotEmpty({message: "Surname is required"})
    @IsString({message: "Invalid string"})
    @Length(2, 50, {message: "Surname must be between 2 and 50 characters"})
    readonly Surname: string;

    @IsNotEmpty({message: "Name is required"})
    @IsString({message: "Invalid string"})
    @Length(2, 50, {message: "Name must be between 2 and 50 characters"})
    readonly Name: string;


    @IsNotEmpty({message: "Email is required"})
    @IsEmail({}, {message: "Invalid email"})
    readonly Email: string;

    @IsNotEmpty({message: "Phone is required"})
    @IsPhoneNumber("VN", {message: "Invalid phone number"})
    @Length(10, 11, {message: "Phone number must be between 10 and 11 characters"})
    readonly Phone: string; // Đổi từ number sang string
}

class UpdateContactDTO {

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 50, {message: "Surname must be between 2 and 50 characters"})
    readonly Surname: string;

    @IsOptional()
    @IsString({message: "Invalid string"})
    @Length(2, 50, {message: "Name must be between 2 and 50 characters"})
    readonly Name: string;


    @IsOptional()
    @IsEmail({}, {message: "Invalid email"})
    readonly Email: string;


    @IsOptional()
    @IsPhoneNumber("VN", {message: "Invalid phone number"})
    @Min(10, {message: "Phone number must be at least 10 characters"})
    @Max(11, {message: "Phone number must be at most 11 characters"})
    @IsNumber({}, {message: "Invalid number"})
    readonly Phone: number;
}

export {CreateContactDTO, UpdateContactDTO};