import {UUID} from "crypto";

interface IValue {
    jti: string;
    nameid: UUID;
    role: string;
    email: string;
    unique_name: string;
    // nbf: number;
    // exp: number;
    // iat: number;
    iss: string;
    aud: string;
}

export {IValue};