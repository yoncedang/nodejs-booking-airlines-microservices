import {JWTConfig} from "../JsonWebToken/Config";
import {Request, Response, NextFunction} from "express";
import {ResponseJson} from "../Response/ResponseJson";
import {IValue} from "../Interface/IMiddleware";
import {RedisRepository} from "../Repositories/RedisRepository/RedisRepository";
import {randomUUID, UUID} from "crypto";


const _redisRepository: RedisRepository = new RedisRepository();
const _response: ResponseJson = new ResponseJson();
const _jwtConfig: JWTConfig = new JWTConfig();


class Middleware {

    public async VerificationTokenOrAnonymous(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {
                const anonymousId = req.cookies["AnonymousId"];
                console.log("AnonymousId: ", anonymousId);
                if (!anonymousId || anonymousId === undefined || anonymousId === null) {
                    return _response.Unauthorized({res: res, message: "Unauthorized"});
                } else {
                    req.body.user = {
                        nameid: anonymousId,
                    };
                    next();
                }

            } else {
                await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                    if (err) {
                        return _response.Unauthorized({res, message: err.message});
                    } else {
                        const MapValue: IValue = {
                            jti: value.jti,
                            role: value.role,
                            email: value.email,
                            nameid: value.nameid,
                            unique_name: value.unique_name,
                            iss: value.iss,
                            aud: value.aud,
                        };
                        req.body.user = MapValue;
                        if (req.body.user.role.toLowerCase() === "user" || req.body.user.role.toLowerCase() === "admin") {
                            return next();
                        }
                        return _response.Forbidden({res: res, message: "Forbidden - User only!"});
                    }
                });
            }
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async VerifyToken(req: Request, res: Response, next: NextFunction): Promise<any> {

        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {

                const userId: UUID = randomUUID();
                await _redisRepository.RedisSet(`AnonymousId: ${userId}`, `Anonymous User!`);
                req.body.user = {
                    nameid: userId,
                    role: "Anonymous",
                    email: "Anonymous",
                    unique_name: "Anonymous",
                    jti: "Anonymous",
                    iss: "Anonymous",
                    aud: "Anonymous"
                };

                res.cookie("AnonymousId", req.body.user.nameid, {
                    httpOnly: true,
                    // secure: true,
                    sameSite: "none",
                    maxAge: 1000 * 60 * 60 * 2
                });
                next();
            } else {
                await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                    if (err) {
                        return _response.Unauthorized({res: res, message: err.message});
                    } else {
                        const MapValue: IValue = {
                            jti: value.jti,
                            role: value.role,
                            email: value.email,
                            nameid: value.nameid,
                            unique_name: value.unique_name,
                            iss: value.iss,
                            aud: value.aud,
                        };
                        req.body.user = MapValue;
                        next();
                    }
                });
            }
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async VerificationAuthenticationUser(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {
                return _response.Unauthorized({res: res, message: "Unauthorized!"});
            } else {
                await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                    if (err) {
                        return _response.Unauthorized({res: res, message: err.message});
                    } else {
                        const MapValue: IValue = {
                            jti: value.jti,
                            role: value.role,
                            email: value.email,
                            nameid: value.nameid,
                            unique_name: value.unique_name,
                            iss: value.iss,
                            aud: value.aud,
                        };
                        req.body.user = MapValue;
                        if (req.body.user.role.toLowerCase() === "user") {
                            return next();
                        }
                        return _response.Forbidden({res: res, message: "Forbidden - User only!"});
                    }
                });
            }
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});

        }
    }

    public async VerificationAuthenticationManager(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {
                return _response.Unauthorized({res: res, message: "Unauthorized!"});
            } else {
                await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                    if (err) {
                        return _response.Unauthorized({res: res, message: err.message});
                    } else {
                        const MapValue: IValue = {
                            jti: value.jti,
                            role: value.role,
                            email: value.email,
                            nameid: value.nameid,
                            unique_name: value.unique_name,
                            iss: value.iss,
                            aud: value.aud,
                        };
                        req.body.user = MapValue;
                        if (req.body.user.role.toLowerCase() === "admin" || req.body.user.role.toLowerCase() === "bamboo" || req.body.user.role.toLowerCase() === "vna") {
                            console.log("Role is: ", req.body.user.role.toLowerCase());
                            return next();
                        }
                        return _response.Forbidden({res: res, message: "Forbidden - Manager only!"});
                    }
                });
            }
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});

        }
    }

    public async VerificationAuthenticationUserAndManager(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {
                return _response.Unauthorized({res: res, message: "Unauthorized!"});
            } else {
                await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                    if (err) {
                        return _response.Unauthorized({res: res, message: err.message});
                    } else {
                        const MapValue: IValue = {
                            jti: value.jti,
                            role: value.role,
                            email: value.email,
                            nameid: value.nameid,
                            unique_name: value.unique_name,
                            iss: value.iss,
                            aud: value.aud,
                        };
                        req.body.user = MapValue;
                        if (req.body.user.role.toLowerCase() === "user" || req.body.user.role.toLowerCase() === "admin" || req.body.user.role.toLowerCase() === "bamboo" || req.body.user.role.toLowerCase() === "vna") {
                            return next();
                        }
                        return _response.Forbidden({res: res, message: "Forbidden - User or Manager only!"});
                    }
                });
            }
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});

        }
    }

    public async TokenAuthentication(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const token = req.headers.authorization?.split(" ")[1];

            if (!token) {
                return _response.Unauthorized({res: res, message: "Unauthorized!"});
            }

            await _jwtConfig.VerificationToken(token, async (err: any, value: IValue) => {
                if (err) {
                    return _response.Unauthorized({res: res, message: err.message});
                } else {
                    const MapValue: IValue = {
                        jti: value.jti,
                        role: value.role,
                        email: value.email,
                        nameid: value.nameid,
                        unique_name: value.unique_name,
                        iss: value.iss,
                        aud: value.aud,
                    };
                    req.body.user = MapValue;
                    return next();
                }
            });

        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

}

export {Middleware};