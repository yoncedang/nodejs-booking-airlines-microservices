class OperationResults<T> {
    public _Data: T;
    public _IsSuccess: boolean;
    public _Message: string;

    // success
    public Success<T>(data: T, message: string): OperationResults<T> {
        const operationResults = new OperationResults<T>();
        operationResults._Data = data;
        operationResults._IsSuccess = true;
        operationResults._Message = message;
        return operationResults;
    }

    public Fail<T>(message: string): OperationResults<T> {
        const operationResults = new OperationResults<T>();
        operationResults._IsSuccess = false;
        operationResults._Message = message;
        return operationResults;
    }

}


export {OperationResults};