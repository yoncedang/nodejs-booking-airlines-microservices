import {Redis} from "ioredis";
import {RedisClient} from "../Config/Config";


const _redis = RedisClient;

class RedisClass {
    public redis: Redis;


    constructor() {
        this.redis = new Redis({
            port: _redis.PORT,
            host: _redis.HOST,
            // password: this.RedisClient.PSW,
        });
        this.connect();
    }

    private connect(): void {
        this.redis.on("connect", () => {
            console.log("Connected to Redis");
        });

        this.redis.on("error", (error: any) => {
            console.error("Error connecting to Redis:", error.message);
        });
    }


}

export {
    RedisClass
};