import {ConfirmedRepository} from "../Repositories/ConfirmedRepositoy/ConfirmedRepository";
import {ResponseJson} from "../Response/ResponseJson";
import {ConfirmedDTO} from "../DTO/ConfirmedDTO";
import {plainToClass} from "class-transformer";
import {validate} from "class-validator";
import {Request, Response} from "express";
import {HelperConfirmed} from "../Repositories/ConfirmedRepositoy/HelperConfirmed";
import {UUID} from "crypto";

const _response: ResponseJson = new ResponseJson();
const _confirmedRepository: ConfirmedRepository = new ConfirmedRepository();
const _helperConfirmed: HelperConfirmed = new HelperConfirmed();

class ConfirmedController {
    public async GetConfrimedByCode(req: Request, res: Response): Promise<Response> {
        try {
            const ticketCode: string = req.params.code;

            const result = await _helperConfirmed.FindOneByTicketCode(ticketCode);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Success"}, result._Data);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async FindListConfirmed(req: Request, res: Response): Promise<Response> {
        try {
            const userId: UUID = req.body.user.nameid;

            const result = await _helperConfirmed.FindConfirmListByUser(userId);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Success"}, result._Data);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async FindOneById(req: Request, res: Response): Promise<Response> {
        try {
            const confirmId: UUID = <UUID>req.params.id;

            const result = await _helperConfirmed.FindOneById(confirmId);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Success"}, result._Data);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }
}

export {ConfirmedController};