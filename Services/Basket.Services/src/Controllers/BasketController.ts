import {Request, Response} from "express";
import {BasketRepository} from "../Repositories/BasketRepository/BasketRepository";
import {ResponseJson} from "../Response/ResponseJson";
import {CreateBasketDTO} from "../DTO/BasketDTO";
import {UUID} from "crypto";
import {HelperBasket} from "../Repositories/BasketRepository/HelperBasket";
import {KafkaProducer} from "../MessageCustomer/Producer";
import {ProducerTOPIC} from "../MessageCustomer/TOPIC/ProducerTOPIC";
import {validate} from "class-validator";
import {plainToClass} from "class-transformer";
import {HelperCustomer} from "../Repositories/CustomerRepository/HelperCustomer";
import {HelperContact} from "../Repositories/ContactRepository/HelperContact";

//

const _response: ResponseJson = new ResponseJson();
const _helperBasket: HelperBasket = new HelperBasket();
const _basketRepository: BasketRepository = new BasketRepository();
const _kafkaProducer: KafkaProducer = new KafkaProducer();
const _producerTOPIC: ProducerTOPIC = new ProducerTOPIC();
const _helperCustomer: HelperCustomer = new HelperCustomer();
const _helperContact: HelperContact = new HelperContact();


class BasketController {
    public async Create(req: Request, res: Response): Promise<Response> {
        try {
            const createBasketDTO: CreateBasketDTO = plainToClass(CreateBasketDTO, req.body);
            const validationErrors = await validate(createBasketDTO);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            const userId: UUID = req.body.user.nameid;
            console.log(userId);


            await _basketRepository.DeleteBasketList(userId);

            const basketData = await _helperBasket.CreateBasketHelper(userId, createBasketDTO);
            if (basketData._IsSuccess === false) {
                return _response.BadRequest({res, message: basketData._Message});
            }
            const result = await _basketRepository.CreateSync(basketData._Data);
            if (result === null) {
                return _response.BadRequest({res, message: "Error while creating basket"});
            }
            await _kafkaProducer.sendMessage(_producerTOPIC.BasketAddingTopic, result.Id, {
                role: req.body.user.role,
                basket: result
            });

            return _response.Success({res, message: "Basket Created"}, result);

        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetAllBasketByUserId(req: Request, res: Response): Promise<Response> {
        try {
            const userId: UUID = req.body.user.nameid;
            const result = await _basketRepository.FindAllBasketByUser(userId);
            if (result === null || result.length === 0) {
                return _response.BadRequest({res, message: "Basket not found"});
            }
            return _response.Success({res, message: "Basket Found"}, result);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetBasketById(req: Request, res: Response): Promise<Response> {
        try {
            const basketId = <UUID>req.params.id;
            const userId: UUID = req.body.user.nameid;
            const results = await _basketRepository.FindAllBasketByUser(userId);
            if (results === null || results.length === 0) {
                return _response.BadRequest({res, message: "You don't have any basket"});
            }

            for (const basket of results) {
                if (basket.Id === basketId) {
                    return _response.Success({res, message: "Basket Found"}, basket);
                }
            }

            return _response.BadRequest({res, message: "Basket not found"});
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async CurrentBasket(req: Request, res: Response): Promise<Response> {
        try {
            const userId: UUID = req.body.user.nameid;
            const result = await _basketRepository.FindOneIsPending(userId);
            if (result === null) {
                return _response.BadRequest({res, message: "Basket not found"});
            }
            return _response.Success({res, message: "Current basket"}, result);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

}

export {BasketController};