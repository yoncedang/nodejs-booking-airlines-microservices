import {PronounsRepository} from "../Repositories/PronounsRepository/PronounsRepository";
import {UUID} from "crypto";
import {Request, Response} from "express";
import {ResponseJson} from "../Response/ResponseJson";
import {UpdatePronounsDTO, CreatePronounsDTO} from "../DTO/PronounsDTO";
import {Pronouns} from "../Entities/Pronouns";
import {validate} from "class-validator";
import {plainToClass} from "class-transformer";

const _pronounsRepository: PronounsRepository = new PronounsRepository();
const _response: ResponseJson = new ResponseJson();

class PronounsController {

    public async CreatePronouns(req: Request, res: Response): Promise<Response> {

        try {
            const pronouns: CreatePronounsDTO = plainToClass(CreatePronounsDTO, req.body);
            const validationErrors = await validate(pronouns);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            const role = req.body.user.role;
            if (role.toLowerCase() !== "admin") {
                return _response.Unauthorized({res: res, message: "Only Admin can delete pronouns"});
            }

            const newPronouns = new Pronouns();
            newPronouns.Pronoun = pronouns.Pronoun;
            newPronouns.Abbreviation = pronouns.Abbreviation;

            const result = await _pronounsRepository.CreatePronouns(newPronouns);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: result._Message}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async DeletePronouns(req: Request, res: Response): Promise<Response> {
        try {
            const id = <UUID>req.params.id;
            const role = req.body.user.role;
            if (role.toLowerCase() !== "admin") {
                return _response.Unauthorized({res: res, message: "Only Admin can delete pronouns"});
            }
            const result = await _pronounsRepository.DeletePronouns(id);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Pronouns deleted"}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetOnePronouns(req: Request, res: Response): Promise<Response> {
        try {
            const id = <UUID>req.params.id;

            const result = await _pronounsRepository.GetOnePronouns(id);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Pronouns deleted"}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetPronouns(req: Request, res: Response): Promise<Response> {
        try {
            const result = await _pronounsRepository.GetPronouns();
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Pronouns deleted"}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async UpdatePronouns(req: Request, res: Response): Promise<Response> {
        try {
            const id = <UUID>req.params.id;
            const role = req.body.user.role;
            const pronouns: UpdatePronounsDTO = plainToClass(UpdatePronounsDTO, req.body);
            const validationErrors = await validate(pronouns);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            if (role.toLowerCase() !== "admin") {
                return _response.Unauthorized({res: res, message: "Only Admin can delete pronouns"});
            }
            const pronounCheck = await _pronounsRepository.GetOnePronouns(id);
            if (pronounCheck._IsSuccess === false) {
                return _response.BadRequest({res, message: pronounCheck._Message});
            }

            const pronoun: Pronouns = pronounCheck._Data;
            pronoun.Pronoun = pronouns.Pronoun;
            pronoun.Abbreviation = pronouns.Abbreviation;

            const result = await _pronounsRepository.UpdatePronouns(id, pronoun);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: "Pronouns updated"}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }
}

export {PronounsController};