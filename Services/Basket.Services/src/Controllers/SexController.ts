import {SexRepository} from "../Repositories/SexRepository/SexRepository";
import {UUID} from "crypto";
import {Request, Response} from "express";
import {ResponseJson} from "../Response/ResponseJson";
import {CreateSexDTO} from "../DTO/SexDTO";
import {Sex} from "../Entities/Sex";
import {validate} from "class-validator";
import {plainToClass} from "class-transformer";
import {UpdatePronounsDTO} from "../DTO/PronounsDTO";


const _sexRepository: SexRepository = new SexRepository();
const _response: ResponseJson = new ResponseJson();


class SexController {

    public async CreateSex(req: Request, res: Response): Promise<Response> {
        try {
            const role = req.body.user.role;
            const sex: CreateSexDTO = plainToClass(CreateSexDTO, req.body);
            const validationErrors = await validate(sex);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            if (role.toLowerCase() !== "admin") {
                return _response.Unauthorized({res: res, message: "Only Admin can create Sex"});
            }

            const newSex = new Sex();
            newSex.Name = sex.Name;

            const createdSex = await _sexRepository.CreateSync(newSex);
            if (createdSex._IsSuccess === false) {
                return _response.BadRequest({res, message: createdSex._Message});
            }
            return _response.Success({res, message: createdSex._Message}, createdSex._Data);

        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async DeleteSex(req: Request, res: Response): Promise<Response> {
        try {
            const id = <UUID>req.params.id;
            const role = req.body.user.role;
            if (role.toLowerCase() !== "admin") {
                return _response.Unauthorized({res: res, message: "Only Admin can delete Sex"});
            }
            const checkSex = await _sexRepository.GetOneSync(id);
            if (checkSex._IsSuccess === false) {
                return _response.NotFound({res, message: checkSex._Message});
            }
            const result = await _sexRepository.DeleteSync(<UUID>checkSex._Data.Id);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: result._Message}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetOneSex(req: Request, res: Response): Promise<Response> {
        try {
            const id = <UUID>req.params.id;
            const result = await _sexRepository.GetOneSync(id);
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: result._Message}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetAllSex(req: Request, res: Response): Promise<Response> {
        try {
            const result = await _sexRepository.GetSync();
            if (result._IsSuccess === false) {
                return _response.BadRequest({res, message: result._Message});
            }

            return _response.Success({res, message: result._Message}, result._Data);
        } catch (error: any) {
            console.log("Error: ", error.message);
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async UpdateSex(req: Request, res: Response): Promise<Response> {
        const id = <UUID>req.params.id;
        const updateSex: CreateSexDTO = plainToClass(CreateSexDTO, req.body);
        const validationErrors = await validate(updateSex);
        if (validationErrors.length > 0) {
            const errorMessages = validationErrors.map(error => error.constraints);

            return _response.BadRequest({res, message: errorMessages});
        }
        const role = req.body.user.role;
        if (role.toLowerCase() !== "admin") {
            return _response.Unauthorized({res: res, message: "Only Admin can update Sex"});
        }

        const checkSex = await _sexRepository.GetOneSync(id);
        if (checkSex._IsSuccess === false) {
            return _response.NotFound({res, message: checkSex._Message});
        }

        checkSex._Data.Name = updateSex.Name;
        checkSex._Data.UpdateAt = new Date();

        const updateResults = await _sexRepository.UpdateSync(id, checkSex._Data);
        if (updateResults._IsSuccess === false) {
            return _response.BadRequest({res, message: updateResults._Message});
        }
        return _response.Success({res, message: updateResults._Message}, updateResults._Data);
    }
}

export {SexController};