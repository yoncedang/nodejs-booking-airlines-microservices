import {HelperContact} from "../Repositories/ContactRepository/HelperContact";
import {Request, Response} from "express";
import {CreateContactDTO, UpdateContactDTO} from "../DTO/ContactDTO";
import {ResponseJson} from "../Response/ResponseJson";
import {RedisRepository} from "../Repositories/RedisRepository/RedisRepository";
import {OperationResults} from "../OperationResults/OperationResults";
import {Contact} from "../Entities/Contact";
import {Basket} from "../Entities/Basket";
import {ContactRepository} from "../Repositories/ContactRepository/ContactRepository";
import {BasketRepository} from "../Repositories/BasketRepository/BasketRepository";
import {UUID} from "crypto";
import {validate} from "class-validator";
import {plainToClass} from "class-transformer";
import {CreateBasketDTO} from "../DTO/BasketDTO";


const _response: ResponseJson = new ResponseJson();
const _contactRepository: ContactRepository = new ContactRepository();
const _helperContact: HelperContact = new HelperContact();
const _basketRepository: BasketRepository = new BasketRepository();

class ContactController {


    public async Create(req: Request, res: Response): Promise<Response> {
        try {
            const createContactDTO: CreateContactDTO = plainToClass(CreateContactDTO, req.body);
            const validationErrors = await validate(createContactDTO);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);
                console.log("LOI DAY NHA");
                return _response.BadRequest({res, message: errorMessages});
            }
            const userId: UUID = req.body.user.nameid;
            const checkBasket = await _basketRepository.FindOneIsPending(userId);
            if (checkBasket === null) {
                return _response.BadRequest({res, message: "Basket not found"});
            }

            const contactData = await _helperContact.CreateContact(checkBasket, createContactDTO);
            const result = await _contactRepository.CreateSync(contactData._Data);
            if (result === null || result === undefined) {
                return _response.BadRequest({res, message: "Error while creating contact"});
            }

            return _response.Success({res, message: "Contact Created"}, result);

        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async AllContactByUser(req: Request, res: Response): Promise<Response> {
        try {
            const userId: UUID = req.body.user.nameid;

            const checkBasket: Basket[] = await _basketRepository.FindAllBasketByUser(userId);
            if (checkBasket.length === 0) {
                return _response.BadRequest({res, message: "Basket not found"});
            }

            const contactList = await _helperContact.FindContactListByBasket(checkBasket);
            if (contactList._IsSuccess === false) {
                return _response.BadRequest({res, message: contactList._Message});
            }

            return _response.Success({res, message: contactList._Message}, contactList._Data);

        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetContactById(req: Request, res: Response): Promise<Response> {
        try {
            const contactId: UUID = <UUID>req.params.id;
            const contact = await _contactRepository.FindOneSync(contactId);

            if (contact === null) {
                return _response.BadRequest({res, message: "Contact not found"});
            }

            return _response.Success({res, message: "Contact Found"}, contact);

        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async UpdateContactById(req: Request, res: Response): Promise<Response> {

        try {
            const contactId: UUID = <UUID>req.params.id;
            const updateContactDTO: UpdateContactDTO = plainToClass(UpdateContactDTO, req.body);
            const validationErrors = await validate(UpdateContactDTO);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            const userId: UUID = req.body.user.nameid;

            // get userId from cookie

            const checkBasket: Basket[] = await _basketRepository.FindAllBasketByUser(userId);
            const checkContact = await _helperContact.CheckContactListByBasket(contactId, checkBasket);
            if (checkContact._IsSuccess === false) {
                return _response.BadRequest({res, message: checkContact._Message});
            }

            const updateContact = await _helperContact.UpdateContact(contactId, checkContact._Data, updateContactDTO);
            if (updateContact._IsSuccess === false) {
                return _response.BadRequest({res, message: updateContact._Message});
            }

            return _response.Success({res, message: "Contact Updated"}, updateContact._Data);


        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }


    // Update later //
    public async DeleteContactById(req: Request, res: Response): Promise<Response> {

        try {
            const contactId: UUID = <UUID>req.params.id;
            const userId: UUID = req.body.user.nameid;

            const checkBasket: Basket[] = await _basketRepository.FindAllBasketByUser(userId);
            const checkContact = await _helperContact.CheckContactListByBasket(contactId, checkBasket);

            if (checkContact._IsSuccess === false) {
                return _response.BadRequest({res, message: checkContact._Message});
            }

            const result = await _contactRepository.DeleteSync(contactId);
            if (result === false) {
                return _response.BadRequest({res, message: "Error while deleting contact"});
            }

            return _response.Success({res, message: "Contact Deleted"}, result);


        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

}

export {ContactController};