import {ResponseJson} from "../Response/ResponseJson";
import {RedisRepository} from "../Repositories/RedisRepository/RedisRepository";
import {OperationResults} from "../OperationResults/OperationResults";
import {CustomerRepository} from "../Repositories/CustomerRepository/CustomerRepository";
import {HelperCustomer} from "../Repositories/CustomerRepository/HelperCustomer";
import {BasketRepository} from "../Repositories/BasketRepository/BasketRepository";
import {Customer} from "../Entities/Customer";
import {Request, Response} from "express";
import {CreateCustomerDTO, UpdateCustomerDTO} from "../DTO/CustomerDTO";
import {UUID} from "crypto";
import {UpdateContactDTO} from "../DTO/ContactDTO";
import {validate} from "class-validator";
import {plainToClass} from "class-transformer";
import {CreateBasketDTO} from "../DTO/BasketDTO";
import {PronounsRepository} from "../Repositories/PronounsRepository/PronounsRepository";
import {SexRepository} from "../Repositories/SexRepository/SexRepository";


const _response: ResponseJson = new ResponseJson();
const _customerRepository: CustomerRepository = new CustomerRepository();
const _helperCustomer: HelperCustomer = new HelperCustomer();
const _basketRepository: BasketRepository = new BasketRepository();
const _pronounsRepository: PronounsRepository = new PronounsRepository();
const _sexRepository: SexRepository = new SexRepository();

class CustomerController {
    public async Create(req: Request, res: Response): Promise<Response> {
        try {
            const createCustomerDTO: CreateCustomerDTO[] = req.body.map((item: any) => plainToClass(CreateCustomerDTO, item));
            const validationErrors = await validate(createCustomerDTO);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }
            const userId: UUID = req.body.user.nameid;

            const checkBasket = await _basketRepository.FindOneIsPending(userId);
            if (checkBasket === null) {
                return _response.BadRequest({res, message: "Basket not found"});
            }
            if (createCustomerDTO.length != checkBasket.AmountPassenger) {
                return _response.BadRequest({
                    res,
                    message: `Amount of passenger is ${checkBasket.AmountPassenger} passenger - But you send request ${createCustomerDTO.length} passenger`
                });
            }

            const listCustomer: Customer[] = [];

            for (const customerDTO of createCustomerDTO) {
                const checkSexId = await _sexRepository.GetOneSync(customerDTO.SexId);
                if (checkSexId._IsSuccess === false) {
                    return _response.BadRequest({res, message: checkSexId._Message});
                }

                const checkPronounsId = await _pronounsRepository.GetOnePronouns(customerDTO.PronounsId);
                if (checkPronounsId._IsSuccess === false) {
                    return _response.BadRequest({res, message: checkPronounsId._Message});
                }

                const createCustomer = await _helperCustomer.CreateCustomer(checkBasket, customerDTO, checkSexId._Data, checkPronounsId._Data);
                const resultsCreate = await _customerRepository.CreateSync(createCustomer._Data);
                if (resultsCreate === null || resultsCreate === undefined) {
                    return _response.BadRequest({res, message: "Error while creating customer"});
                }

                listCustomer.push(resultsCreate);
            }

            return _response.Success({res, message: "Customer Created"}, listCustomer);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetAllCustomerOfBasketSuccess(req: Request, res: Response): Promise<Response> {
        try {
            const userId: UUID = req.body.user.nameid;
            const basketId = <UUID>req.body.id;
            const checkBasket = await _basketRepository.FindAllBasketByUser(userId);

            for (const basket of checkBasket) {
                if (basket.Id === basketId) {
                    const results = await _customerRepository.FindAllSync(basketId);
                    if (results === null || results.length === 0) {
                        return _response.BadRequest({res, message: "Customer not found"});
                    }
                    return _response.Success({res, message: "Customer Found"}, results);
                }
            }
            return _response.BadRequest({res, message: "Basket not found"});
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async UpdateCustomerIdByFlightMananger(req: Request, res: Response): Promise<Response> {
        try {

            const customerId: UUID = <UUID>req.params.id;
            const updateCustomerDTO: UpdateCustomerDTO = plainToClass(UpdateCustomerDTO, req.body);

            const validationErrors = await validate(updateCustomerDTO);
            if (validationErrors.length > 0) {
                const errorMessages = validationErrors.map(error => error.constraints);

                return _response.BadRequest({res, message: errorMessages});
            }

            const checkCustomer = await _helperCustomer.CheckCustomerIdSync(customerId);
            if (checkCustomer._IsSuccess === false) {
                return _response.BadRequest({res, message: checkCustomer._Message});
            }
            const checkSex = await _helperCustomer.CheckSexSync(updateCustomerDTO.SexId);
            if (checkSex._IsSuccess === false) {
                return _response.BadRequest({res, message: checkSex._Message});
            }

            const UpdateCustomerSync = await _helperCustomer.UpdateCustomerIdSync(checkCustomer._Data, updateCustomerDTO);
            const results = await _customerRepository.UpdateSync(customerId, UpdateCustomerSync._Data);
            if (results === null) {
                return _response.BadRequest({res, message: "Error while updating customer"});
            }
            return _response.Success({res, message: "Customer Updated"}, results);

        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }

    public async GetCustomerById(req: Request, res: Response): Promise<Response> {
        try {
            const customerId: UUID = <UUID>req.params.id;
            const results = await _customerRepository.FindOneSync(customerId);
            if (results === null) {
                return _response.BadRequest({res, message: "Customer not found"});
            }
            return _response.Success({res, message: "Customer Found"}, results);
        } catch (error: any) {
            return _response.InternalServerError({res: res, message: error.message});
        }
    }


}

export {CustomerController};