import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import {Customer} from "./Customer";

@Entity()
export class Sex {
    @PrimaryGeneratedColumn("uuid")
    Id: string;

    @Column()
    Name: string;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    CreateAt: Date;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    UpdateAt: Date;

    @Column({default: true})
    Status: boolean;

    @OneToMany(() => Customer, customer => customer.Sex)
    Customer: Customer[];


}