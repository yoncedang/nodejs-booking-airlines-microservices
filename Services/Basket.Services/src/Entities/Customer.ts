import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn, OneToOne} from "typeorm";
import {Pronouns} from "./Pronouns";
import {Basket} from "./Basket";

import {Sex} from "./Sex";
import {UUID} from "crypto";

@Entity()
export class Customer {
    @PrimaryGeneratedColumn("uuid")
    Id: string;

    @ManyToOne(() => Basket, basket => basket.Customer)
    @JoinColumn({name: "BasketId"})
    Basket: UUID;

    @ManyToOne(() => Sex, sex => sex.Customer)
    @JoinColumn({name: "SexId"})
    Sex: UUID;

    @Column()
    Surname: string;

    @Column({nullable: true})
    MiddleName: string;

    @Column()
    Name: string;

    @Column()
    DateOfBirth: Date;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    CreateAt: Date;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    UpdateAt: Date;

    @ManyToOne(() => Pronouns, pronouns => pronouns.Customer)
    @JoinColumn({name: "PronounsId"})
    Pronouns: UUID;

}