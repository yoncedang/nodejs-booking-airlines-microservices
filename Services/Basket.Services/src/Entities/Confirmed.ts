import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Unique, OneToMany} from "typeorm";
import {Basket} from "./Basket";
import {UUID} from "crypto";


@Entity()
@Unique(["TicketCode"])
export class Confirmed {
    @PrimaryGeneratedColumn("uuid")
    Id: string;

    @Column()
    TicketCode: string;

    @Column()
    UserId: UUID;

    @ManyToOne(() => Basket, basket => basket.Confirmed)
    @JoinColumn({name: "BasketId"})
    Basket: UUID;
}