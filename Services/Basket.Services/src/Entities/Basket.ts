import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne} from "typeorm";
import {Customer} from "./Customer";
import {Contact} from "./Contact";
import {UUID} from "crypto";
import {Length} from "class-validator";
import {Confirmed} from "./Confirmed";

@Entity()
export class Basket {
    @PrimaryGeneratedColumn("uuid")
    Id: UUID;

    @Column({nullable: true})
    UserId: UUID;

    @Column()
    FlightComeId: UUID;

    @Column({nullable: true})
    FlightBackId: UUID;

    @Column({nullable: false})
    AmountPassenger: number;

    @Column()
    AmountTicket: number;

    @Column({type: "float"})
    Price: number;

    @Column()
    AirlinesCome: string;

    @Column({nullable: true})
    AirlinesBack: string;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    CreateAt: Date;

    @Column({default: () => "CURRENT_TIMESTAMP"})
    UpdateAt: Date;

    @Column({default: true})
    isPending: boolean;

    @Column({default: "Pending"})
    Status: string;

    @OneToMany(() => Customer, customer => customer.Basket)
    Customer: Customer[];

    @OneToMany(() => Contact, contact => contact.Basket)
    Contact: Contact[];

    @OneToMany(() => Confirmed, confirmed => confirmed.Basket)
    Confirmed: Confirmed[];

}

