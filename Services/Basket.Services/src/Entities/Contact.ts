import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from "typeorm";
import {Customer} from "./Customer";
import {Basket} from "./Basket";
import {UUID} from "crypto";

@Entity()
export class Contact {
    @PrimaryGeneratedColumn("uuid")
    Id: string;

    @ManyToOne(() => Basket, basket => basket.Customer)
    @JoinColumn({name: "BasketId"})
    Basket: UUID;

    @Column()
    Surname: string;

    @Column()
    Name: string;

    @Column()
    Email: string;

    @Column()
    Phone: string;

    // Date
    @Column({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    CreatedAt: Date;

    // Date
    @Column({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    UpdatedAt: Date;


}