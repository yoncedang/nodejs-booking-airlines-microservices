import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany} from "typeorm";
import {Customer} from "./Customer";

@Entity()
export class Pronouns {
    @PrimaryGeneratedColumn("uuid")
    Id: string;

    @Column()
    Pronoun: string;

    @Column()
    Abbreviation: string;


    @OneToMany(() => Customer, customer => customer.Pronouns)
    Customer: Customer[];

}