import dotenv from "dotenv";

dotenv.config();


const API_PORT = process.env.API_PORT || 8400;


const RedisClient: any = {
    HOST: process.env.REDIS_HOST,
    PORT: process.env.REDIS_PORT,
    PSW: process.env.REDIS_PASSWORD,

};

const JWT_Configuration: any = {
    JWT_SECRET: process.env.JWT_SECRET,
    JWT_ISSUER: process.env.JWT_ISSUER,
    JWT_AUDIENCE: process.env.JWT_AUDIENCE,
};

const DataSouceDB: any = {
    type: process.env.TYPE,
    host: process.env.HOST,
    port: process.env.PORT,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
};

export {
    API_PORT,
    RedisClient,
    JWT_Configuration,
    DataSouceDB
};


