import {Response} from "express";


interface typeResponseJson {
    res: Response;
    message: any;
}

interface IResponseJson {
    BadRequest: ({res, message}: typeResponseJson) => Response;
    Unauthorized: ({res, message}: typeResponseJson) => Response;
    Forbidden: ({res, message}: typeResponseJson) => Response;
    NotFound: ({res, message}: typeResponseJson) => Response;
    InternalServerError: ({res, message}: typeResponseJson) => Response;
    Success: ({res, message}: typeResponseJson, data: any) => Response;
    GeneralSuccess: ({res, message}: typeResponseJson) => Response;

}


export {IResponseJson, typeResponseJson};