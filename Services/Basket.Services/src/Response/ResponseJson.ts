import {Response} from "express";
import {typeResponseJson, IResponseJson} from "./IResponseJson";


class ResponseJson implements IResponseJson {

    public BadRequest({res, message}: typeResponseJson): Response {
        return res.status(400).json({isSuccess: false, message: message});
    };

    public Unauthorized({res, message}: typeResponseJson): Response {
        return res.status(401).json({isSuccess: false, message: message});
    };

    public Forbidden({res, message}: typeResponseJson): Response {
        return res.status(403).json({isSuccess: false, message: message});
    };

    public NotFound({res, message}: typeResponseJson): Response {
        return res.status(404).json({isSuccess: false, message: message});
    };

    public InternalServerError({res, message}: typeResponseJson): Response {
        return res.status(500).json({isSuccess: false, InternalServerError: true, message: message});
    };

    public Success({res, message}: typeResponseJson, data: any): Response {
        return res.status(200).json({isSuccess: true, message: message, data: data});
    };

    public GeneralSuccess({res, message}: typeResponseJson): Response {
        return res.status(200).json({isSuccess: true, message: message});
    };
}

export {ResponseJson};