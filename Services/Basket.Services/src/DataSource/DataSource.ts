import {DataSource} from "typeorm";

import {Pronouns} from "../Entities/Pronouns";
import {DataSouceDB} from "../Config/Config";
import {Sex} from "../Entities/Sex";

class DbSource {
    private readonly _dataSourceDB = DataSouceDB;

    public _dataSource: DataSource;

    constructor() {
        this._dataSource = new DataSource({
            type: this._dataSourceDB.type || "mysql",
            host: this._dataSourceDB.host,
            port: this._dataSourceDB.port,
            username: "root",
            password: this._dataSourceDB.password,
            database: this._dataSourceDB.database,
            synchronize: true,
            logging: true,
            entities: [
                "src/Entities/**/*.ts"
            ],
            migrations: [
                "src/Migrations/**/*.ts"
            ],
            subscribers: [
                "src/Subscribers/**/*.ts"
            ],
        });
        this.createConnection();
    }


    private async createConnection(): Promise<void> {
        try {
            const _connection = await this._dataSource.initialize();

            if (_connection) {
                console.log("Connection successful");
                await Promise.all([
                    this.seedPronouns(),
                    this.seedSex()
                ]);
            }
        } catch (e: any) {
            console.log("Connection failed");
            console.log(e.message);
        }
    }


    private async seedPronouns(): Promise<void> {
        try {
            if (!this._dataSource) {
                return console.log("Can't seed pronouns - connection failed");
            }
            const pronounsRepository = this._dataSource.getRepository(Pronouns);
            const pronounsToSeed = [
                {Id: "6d7a78d4-b955-4366-9717-e796edfa3854", Pronoun: "Ông/Mr.", Abbreviation: "Mr."},
                {Id: "ba4e9c49-4723-4ba3-af5d-764b0cdfde6d", Pronoun: "Bà/Mrs.", Abbreviation: "Mrs."},
                {Id: "8ea17abb-3ff6-4e79-84d8-b9ff12c85b05", Pronoun: "Cô/Ms.", Abbreviation: "Ms."},
                {Id: "54dff6d1-618d-42e9-95ca-49db7eb35e77", Pronoun: "Anh/Mr.", Abbreviation: "Mr."},
                {Id: "479c24ee-0cff-4fe2-85ba-7301ea691717", Pronoun: "Chị/Mrs.", Abbreviation: "Mrs."},
                {Id: "74328dce-4e1d-420f-be2d-1a2db0881fd5", Pronoun: "Em/Ms.", Abbreviation: "Ms."},
            ];
            for (const pronounData of pronounsToSeed) {
                const existingPronoun = await pronounsRepository.findOne({where: {Id: pronounData.Id}});
                if (!existingPronoun) {
                    const newPronoun = pronounsRepository.create({
                        Id: pronounData.Id,
                        Pronoun: pronounData.Pronoun,
                        Abbreviation: pronounData.Abbreviation
                    });
                    await pronounsRepository.save(newPronoun);
                    console.log(`Pronoun with Id ${pronounData.Id} seeded successfully`);
                } else {
                    console.log(`Pronoun with Id ${pronounData.Id} already exists, skipping seeding`);
                }
            }
        } catch (e: any) {
            console.log("Error seeding pronouns");
            console.log(e.message);
        }
    }

    private async seedSex(): Promise<void> {
        try {
            if (!this._dataSource) {
                return console.log("Can't seed pronouns - connection failed");
            }
            const sexRepository = this._dataSource.getRepository(Sex);

            const sexToSeed = [
                {Id: "aa4ff267-d536-4f52-b811-140977449776", Name: "Male"},
                {Id: "f9f09ef7-826b-403e-a545-cd51c520c8db", Name: "Female"},
            ];
            for (const sexData of sexToSeed) {
                const existingSex = await sexRepository.findOne({where: {Id: sexData.Id}});
                if (!existingSex) {
                    const newSex = sexRepository.create({
                        Id: sexData.Id,
                        Name: sexData.Name
                    });
                    await sexRepository.save(newSex);
                } else {
                    console.log(`Sex with Id ${sexData.Id} already exists, skipping seeding`);
                }
            }
            console.log("Sex seeded successfully");
        } catch (e: any) {
            console.log("Error seeding Sex");
            console.log(e.message);
        }
    }
}

export {DbSource};
