import express, {Application, Request, Response} from "express";
import cors from "cors";
import {API_PORT} from "./Config/Config";
import compression from "compression";
import cookieParser from "cookie-parser";
import {KafkaConfig} from "../src/MessageCustomer/KafkaConfig";
import {BasketRoutes} from "./Routes/BasketRoutes";
import {ContactRoutes} from "./Routes/ContactRoutes";
import {CustomerRoutes} from "./Routes/CustomerRoutes";
import {PronounsRoutes} from "./Routes/PronounsRoutes";
import {SexRoutes} from "./Routes/SexRoutes";
import {ConfirmedRoutes} from "./Routes/ConfirmedRoutes";
import {RedisClass} from "./Redis/Redis";
import open from "open";
import "reflect-metadata";
import {DbSource} from "./DataSource/DataSource";
import * as swaggerUi from "swagger-ui-express";
import {KafkaConsumer} from "./MessageCustomer/Consumer";


import fs from "fs";
import https from "https";
import path from "path";
import YAML from "yamljs";


class App {
    public app: Application;
    public port: string | number;
    private readonly _consumer: KafkaConsumer;
    private readonly _basket: BasketRoutes;
    private readonly _contact: ContactRoutes;
    private readonly _customer: CustomerRoutes;
    private readonly _pronouns: PronounsRoutes;
    private readonly _confirmed: ConfirmedRoutes;
    private readonly _sex: SexRoutes;
    private readonly _filePath = fs.readFileSync(path.resolve("SwaggerConfig.json"), "utf-8");
    private readonly _swaggerDocument = JSON.parse(this._filePath);


    constructor() {
        this.app = express();
        this.port = API_PORT;
        this._consumer = new KafkaConsumer();
        this._basket = new BasketRoutes();
        this._contact = new ContactRoutes();
        this._customer = new CustomerRoutes();
        this._pronouns = new PronounsRoutes();
        this._confirmed = new ConfirmedRoutes();
        this._sex = new SexRoutes();
        this.Httpslisten();
        this.Httplisten();
        this.Middlewares();
        this.appRoutes();
        this.RegisterConsumer();
        this.connect();


    }

    private Middlewares(): void {
        this.app.use(express.static("."));
        this.app.use(express.urlencoded({extended: false}));
        this.app.use(express.json());
        // cors allow 7999 port
        this.app.use(cors({origin: "http://localhost:8401", credentials: true}));
        this.app.use(compression());
        this.app.use(cookieParser());
    }

    private appRoutes(): void {

        this.app.get("/", (req: Request, res: Response) => {
            // Redirect to Swagger
            res.redirect("/swagger/index.html");
            // res.send("Hello World! Https");
        });
        // this.app.use("/swagger/index.html", swaggerUi.serve, swaggerUi.setup(this._swaggerDocument));
        this.app.use("/swagger/index.html", swaggerUi.serve, swaggerUi.setup(this._swaggerDocument, {swaggerOptions: {url: "/swagger.json"}}));

        // Endpoint để trả về tệp JSON của Swagger
        this.app.use("/swagger/v1/swagger.json", (req, res) => {
            res.setHeader("Content-Type", "application/json");
            res.send(this._swaggerDocument);
        });

        this.app.use("/api", this._basket.Router);
        this.app.use("/api", this._contact.Router);
        this.app.use("/api", this._customer.Router);
        this.app.use("/api", this._pronouns.Router);
        this.app.use("/api", this._sex.Router);
        this.app.use("/api", this._confirmed.Router);
    }

    private connect(): void {
        new RedisClass();
        new KafkaConfig();
        new DbSource();
    }

    private async RegisterConsumer(): Promise<void> {
        await Promise.all([
            this._consumer.basketCreateCode(),
            this._consumer.paymentCancelBasket(),
        ]);
    }

    private Httpslisten(): void {
        const pfxFilePath = path.join(__dirname, "Certificates", "aspnetapp.pfx"); // Đường dẫn tới tệp PFX
        const pfxPassphrase = "thien1999"; // Thay thế bằng mật khẩu thực tế của bạn, nếu có

        const serverOptions = {
            pfx: fs.readFileSync(pfxFilePath),
            passphrase: pfxPassphrase,
        };

        https
            .createServer(serverOptions, this.app)
            .listen(8401, () => {
                console.log(`Swagger is running on https://localhost:8401/swagger/index.html`);
            });
    }

    private Httplisten(): void {
        this.app.listen(this.port, () => {
            console.log(`Swagger is running on http://localhost:${this.port}/swagger/index.html`);
        });
    }
}

new App();