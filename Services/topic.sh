#!/bin/bash

# Thực hiện tạo các topic Kafka

#TOPIC

docker exec -i kafkaMicroservice kafka-topics --create --topic HoldingTicket --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic PaymentSuccess --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic PaymentCancel --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic VerificationRegister --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic ResendVerificationRegister --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic RequestResetPassword --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic BasketAddingTopic --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic BasketCreateCode --bootstrap-server kafka:29092
docker exec -i kafkaMicroservice kafka-topics --create --topic PaymentCancelBasket --bootstrap-server kafka:29092

