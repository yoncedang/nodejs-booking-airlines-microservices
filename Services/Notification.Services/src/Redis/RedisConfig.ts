import {Redis} from "ioredis";
import {ConfigEnv} from "../Config/ConfigEnv";

class RedisConfig {

    private redis: Redis;
    private RedisClient: ConfigEnv;

    constructor() {
        this.RedisClient = new ConfigEnv();
        this.redis = new Redis({
            port: this.RedisClient.RedisPort,
            host: this.RedisClient.RedisHost,
        });
    }

    public connectRedis(): void {
        this.redis.on("connect", () => {
            console.log("Redis connected");
        });
        this.redis.on("error", (error) => {
            console.log("Redis error: ", error);
        });
    }

    public async RedisSet(key: string, value: any, timeSpan?: number): Promise<void> {
        await this.redis.set(key, JSON.stringify(value), "EX", timeSpan || 31557600000);
        console.log("Set key success: ", key);
    }

    public async RedisGet(key: string): Promise<boolean> {
        const data = await this.redis.get(key);
        return data ? true : false;
    }

    public async RedisDelete(key: string): Promise<boolean> {
        const data = await this.redis.del(key);
        return data ? true : false;
    }
}

export {RedisConfig};