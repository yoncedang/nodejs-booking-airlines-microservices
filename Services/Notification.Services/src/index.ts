import express, {Application, Request, Response} from "express";
import cors from "cors";
import {RedisConfig} from "./Redis/RedisConfig";
import {ConsumerApp} from "./MessageBrokers/Consumer";


class App {
    public app: Application;
    public port: number;
    private redis: RedisConfig;
    private consumer: ConsumerApp;

    constructor(port: number) {
        this.app = express();
        this.port = port;
        this.consumer = new ConsumerApp();
        this.redis = new RedisConfig();

        this.middlewares();
        this.routes();
        this.listen();
        this.run();

        this.RegisterConsumer();
    }

    private middlewares(): void {
        this.app.use(express.json());
        this.app.use(cors());
        this.app.use(express.urlencoded({extended: false}));
    }

    private run(): void {
        this.redis.connectRedis();

    }


    private routes(): void {
        this.app.use("/", (req: Request, res: Response) => {
            res.send("Hello World! This is Notification.Service");
        });
    }

    private async RegisterConsumer(): Promise<void> {
        await Promise.all([
            this.consumer.verificationRegister(),
            this.consumer.resendVerificationRegister(),
            this.consumer.requestResendPassword(),
        ]);
    }

    private listen(): void {
        this.app.listen(this.port, () => {
            console.log(`App listening on the port ${this.port}`);
        });
    }

}

new App(8500);