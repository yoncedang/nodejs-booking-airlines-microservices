import ConsumerTopic from "./TOPIC/ConsumerTopic";
import {Consumer} from "kafkajs";
import {KafkaConfig} from "./KafkaConfig";
import {EmailSending} from "../NodeMailer/NodeMailer";
import {RedisConfig} from "../Redis/RedisConfig";


class ConsumerApp extends KafkaConfig {

    private Verification_Register: Consumer;
    private Resend_Verification_Register: Consumer;
    private Request_Reset_Password: Consumer;
    private TopicConsumer: any = ConsumerTopic;
    private EmailSending: EmailSending;
    private RedisConfig: RedisConfig;

    constructor() {
        super();

        this.EmailSending = new EmailSending();
        this.RedisConfig = new RedisConfig();

        // GroupId is Notification.Service - and auto create topic if not exists
        this.Verification_Register = this.kafka.consumer({
            groupId: this.TopicConsumer.Verification_Register,
            allowAutoTopicCreation: true
        });
        this.Resend_Verification_Register = this.kafka.consumer({
            groupId: this.TopicConsumer.Resend_Verification_Register,
            allowAutoTopicCreation: true
        });
        this.Request_Reset_Password = this.kafka.consumer({
            groupId: this.TopicConsumer.Request_Reset_Password,
            allowAutoTopicCreation: true
        });
        this.connect();
        this.subscribe();
    }

    // Connect to Kafka
    private async connect(): Promise<void> {
        // Connect to Kafka - connect đồng thời tới tất cả
        await Promise.all([
            this.Verification_Register.connect(),
            this.Resend_Verification_Register.connect(),
            this.Request_Reset_Password.connect(),
        ]);
    }

    // Subscribe to Kafka
    private async subscribe(): Promise<void> {
        // Subscribe to Kafka - Subscribe đồng thời tới tất cả các topic
        await Promise.all([
            this.Verification_Register.subscribe({
                // Không đọc lại các message cũ - Don't read old messages again => fromBeginning: false
                topic: this.TopicConsumer.Verification_Register, fromBeginning: false
            }),
            this.Resend_Verification_Register.subscribe({
                // Không đọc lại các message cũ - Don't read old messages again => fromBeginning: false
                topic: this.TopicConsumer.Resend_Verification_Register, fromBeginning: false
            }),
            this.Request_Reset_Password.subscribe({
                // Không đọc lại các message cũ - Don't read old messages again => fromBeginning: false
                topic: this.TopicConsumer.Request_Reset_Password, fromBeginning: false
            }),
        ]);
    }

    public async verificationRegister(): Promise<void> {
        await this.Verification_Register.run({
            eachMessage: async ({topic, partition, message}): Promise<void> => {
                // Get data from Kafka Producer- Lấy dữ liệu từ Kafka
                const data = {
                    topic,
                    partition,
                    key: message.key?.toString(),
                    value: message.value?.toString(),
                };
                console.log(JSON.parse(data.value || ""));


                // Check key in Redis - Kiểm tra key trong Redis
                const RedisCheckingKey = await this.RedisConfig.RedisGet(data.key || "");
                // Chuyển dữ liệu từ string sang JSON
                const {
                    email,
                    name,
                    code
                } = JSON.parse(data.value || "");


                if (RedisCheckingKey) {
                    // Don't need async await, set 30 minutes - không cần phải đợi Async await, set 30 phút

                    await Promise.all([
                        this.RedisConfig.RedisSet(code, {Email: email}, 1800),
                    ]);

                    // Send email - Gửi email
                    await this.EmailSending.VerificationEmailRegister({
                        email,
                        name,
                        code
                    });

                    // Commit offset - Gửi lại offset
                    await this.Verification_Register.commitOffsets([{
                        topic: topic,
                        partition: partition,
                        offset: (+message.offset + 1).toString()
                    }]);
                    // Delete key in Redis - Xóa key trong Redis sau khi gửi email thành công và commit offset
                    await this.RedisConfig.RedisDelete(data.key || "");
                    console.log("Send email success");
                }
            },
        });
    }

    public async resendVerificationRegister(): Promise<void> {
        await this.Resend_Verification_Register.run({
            eachMessage: async ({topic, partition, message}): Promise<void> => {
                // Get data from Kafka Producer- Lấy dữ liệu từ Kafka
                const data = {
                    topic,
                    partition,
                    key: message.key?.toString(),
                    value: message.value?.toString(),
                };
                // console.log(JSON.parse(data.value || ""));

                // Check key in Redis - Kiểm tra key trong Redis
                const RedisCheckingKey = await this.RedisConfig.RedisGet(data.key || "");
                // Chuyển dữ liệu từ string sang JSON
                const {
                    email,
                    name,
                    code
                } = JSON.parse(data.value || "");


                if (RedisCheckingKey) {
                    // Don't need async await, set 30 minutes - không cần phải đợi Async await, set 30 phút

                    await Promise.all([
                        this.RedisConfig.RedisSet(code, {Email: email}, 1800),
                    ]);

                    // Send email - Gửi email
                    await this.EmailSending.ResendVerification({
                        email,
                        name,
                        code
                    });

                    // Commit offset - Gửi lại offset
                    await this.Resend_Verification_Register.commitOffsets([{
                        topic: topic,
                        partition: partition,
                        offset: (+message.offset + 1).toString()
                    }]);
                    // Delete key in Redis - Xóa key trong Redis sau khi gửi email thành công và commit offset
                    await this.RedisConfig.RedisDelete(data.key || "");
                    console.log("Send email success");
                }
            },
        });
    }

    public async requestResendPassword(): Promise<void> {
        await this.Request_Reset_Password.run({
            eachMessage: async ({topic, partition, message}): Promise<void> => {
                // Get data from Kafka Producer- Lấy dữ liệu từ Kafka
                const data = {
                    topic,
                    partition,
                    key: message.key?.toString(),
                    value: message.value?.toString(),
                };
                console.log(JSON.parse(data.value || ""));


                // Check key in Redis - Kiểm tra key trong Redis
                const RedisCheckingKey = await this.RedisConfig.RedisGet(data.key || "");
                // Chuyển dữ liệu từ string sang JSON
                const {
                    email,
                    token,
                    localhost,
                    endpoint,
                    name
                } = JSON.parse(data.value || "");


                if (RedisCheckingKey) {
                    // Don't need async await, set 30 minutes - không cần phải đợi Async await, set 30 phút

                    await Promise.all([
                        this.RedisConfig.RedisSet(token, {Email: email}, 86400),
                    ]);

                    // Send email - Gửi email
                    await this.EmailSending.SendingOTP_ResetPassword({
                        email,
                        token,
                        localhost,
                        endpoint,
                        name
                    });

                    // Commit offset - Gửi lại offset
                    await this.Request_Reset_Password.commitOffsets([{
                        topic: topic,
                        partition: partition,
                        offset: (+message.offset + 1).toString()
                    }]);
                    // Delete key in Redis - Xóa key trong Redis sau khi gửi email thành công và commit offset
                    await this.RedisConfig.RedisDelete(data.key || "");
                    console.log("Send email success");
                }
            },
        });
    }
}

export {ConsumerApp};