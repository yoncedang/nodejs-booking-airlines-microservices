import kafkajs, {Kafka} from "kafkajs";


class KafkaConfig {

    protected kafka: Kafka;

    constructor() {
        this.kafka = new kafkajs.Kafka({
            clientId: "Notification.Service",
            brokers: ["kafkaMicroservice:29092"], // Địa chỉ IP của Kafka server và cổng TLS/SSL
            // brokers: ["localhost:9092"], // Địa chỉ IP của Kafka server và cổng TLS/SSL
            // brokers: ["152.42.221.228:9092"], // Địa chỉ IP của Kafka server và cổng TLS/SSL
        });

        this.checkConnection();
    }

    private async checkConnection(): Promise<void> {

        try {
            await this.kafka.admin().connect();
            console.log("Kafka is connected");
        } catch (error: any) {
            console.log("Kafka is not connected", error.message);
        }
    }

}

export {KafkaConfig};