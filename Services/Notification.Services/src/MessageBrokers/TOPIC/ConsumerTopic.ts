const ConsumerTopic = {
    Verification_Register: "VerificationRegister",
    Resend_Verification_Register: "ResendVerificationRegister",
    Request_Reset_Password: "RequestResetPassword",
};

export default ConsumerTopic;