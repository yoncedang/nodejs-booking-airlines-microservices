import dotenv from "dotenv";

dotenv.config();


class ConfigEnv {

    public RedisPort: number;
    public RedisHost: string;


    public EmailUser: string;
    public EmailPassword: string;

    constructor() {
        this.RedisPort = Number(process.env.REDIS_PORT) || 6379;
        this.RedisHost = process.env.REDIS_HOST || "localhost";


        this.EmailUser = process.env.EmailUser || "";
        this.EmailPassword = process.env.EmailPassword || "";
    }
}

export {ConfigEnv};