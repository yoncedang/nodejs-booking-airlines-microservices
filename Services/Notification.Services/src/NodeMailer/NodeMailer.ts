import {MaillerConfig} from "./MaillerConfig";
import {ConfigEnv} from "../Config/ConfigEnv";

interface IVerificationEmailRegister {
    email: string;
    code: string | number;
    name: string;
}

interface IResetPassword {
    email: string;
    token: string;
    localhost: string;
    endpoint: string;
    name: string;
}

var config = new ConfigEnv();

class EmailSending extends MaillerConfig {
    public async VerificationEmailRegister(
        {
            email,
            name,
            code
        }: IVerificationEmailRegister): Promise<void> {

        try {
            const mailOptions = {
                from: config.EmailUser,
                to: email,
                subject: 'Verification Register Email',
                html: `
                    <!DOCTYPE html>
                        <html lang="en">
                          <head>
                            <meta charset="UTF-8" />
                            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                            <meta http-equiv="X-UA-Compatible" content="ie=edge" />
                            <title>Static Template</title>
                        
                            <link
                              href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap"
                              rel="stylesheet"
                            />
                          </head>
                          <body
                            style="
                              margin: 0;
                              font-family: 'Poppins', sans-serif;
                              background: #ffffff;
                              font-size: 14px;
                            "
                          >
                            <main>
                              <div
                                style="
                                  margin: 0;
                                  margin-top: 70px;
                                  padding: 20px 40px;
                                  background: #ffffff;
                                  border-radius: 30px;
                                  text-align: center;
                                "
                              >
                                <div style="width: 100%; max-width: 489px; margin: 0 auto">
                                  <h1
                                    style="margin: 0; font-size: 40px; font-weight: 500; color: #1f1f1f"
                                  >
                                    OTP Verification
                                  </h1>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 17px;
                                      font-size: 25px;
                                      font-weight: 500;
                                      color: #ba3d4f;
                                    "
                                  >
                                    Hey ${name},
                                  </p>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 17px;
                                      font-weight: 500;
                                      letter-spacing: 0.56px;
                                      font-size: 20px;
                                    "
                                  >
                                    Thank you for choosing with us. Just one step, please use the following OTP to
                                    complete the procedure to verification your email address. OTP is valid
                                    for
                                    <span style="font-weight: 600; color: #ba3d4f; font-size: 22px;">30 minutes</strong></span>. Do
                                    not share this code with others.
                                  </p>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 60px;
                                      font-size: 40px;
                                      font-weight: 600;
                                      letter-spacing: 25px;
                                      color: #ba3d4f;
                                    "
                                  >
                                    ${code}
                                  </p>
                                </div>
                              </div>
                        
                              <p
                                style="
                                  max-width: 400px;
                                  margin: 0 auto;
                                  margin-top: 30px;
                                  text-align: center;
                                  font-weight: 500;
                                  color: #8c8c8c;
                                "
                              >
                                Need help? Ask at
                                <a
                                  href="huydang.dev@gmail.com"
                                  style="color: #499fb6; text-decoration: none"
                                  >huydang.dev@gmail.com</a
                                >
                              </p>
                            </main>
                          </body>
                        </html>

`
            };
            await this.GeneralTransporter.sendMail(mailOptions, (error, info): void => {
                if (error) {
                    console.log(error.message);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        } catch (error: any) {
            console.log(error.message);
        }
    }

    public async ResendVerification(
        {
            email,
            name,
            code
        }: IVerificationEmailRegister): Promise<void> {

        try {
            const mailOptions = {
                from: config.EmailUser,
                to: email,
                subject: 'Resend Verification',
                html: `
                    <!DOCTYPE html>
                        <html lang="en">
                          <head>
                            <meta charset="UTF-8" />
                            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                            <meta http-equiv="X-UA-Compatible" content="ie=edge" />
                            <title>Static Template</title>
                        
                            <link
                              href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap"
                              rel="stylesheet"
                            />
                          </head>
                          <body
                            style="
                              margin: 0;
                              font-family: 'Poppins', sans-serif;
                              background: #ffffff;
                              font-size: 14px;
                            "
                          >
                            <main>
                              <div
                                style="
                                  margin: 0;
                                  margin-top: 70px;
                                  padding: 20px 40px;
                                  background: #ffffff;
                                  border-radius: 30px;
                                  text-align: center;
                                "
                              >
                                <div style="width: 100%; max-width: 489px; margin: 0 auto">
                                  <h1
                                    style="margin: 0; font-size: 40px; font-weight: 500; color: #1f1f1f"
                                  >
                                    OTP Verification Resended
                                  </h1>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 17px;
                                      font-size: 25px;
                                      font-weight: 500;
                                      color: #ba3d4f;
                                    "
                                  >
                                    Hey ${name},
                                  </p>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 17px;
                                      font-weight: 500;
                                      letter-spacing: 0.56px;
                                      font-size: 20px;
                                    "
                                  >
                                    Thank you for choosing with us. Just one step, please use the following OTP to
                                    complete the procedure to verification your email address. OTP is valid
                                    for
                                    <span style="font-weight: 600; color: #ba3d4f; font-size: 22px;">30 minutes</strong></span>. Do
                                    not share this code with others.
                                  </p>
                                  <p
                                    style="
                                      margin: 0;
                                      margin-top: 60px;
                                      font-size: 40px;
                                      font-weight: 600;
                                      letter-spacing: 25px;
                                      color: #ba3d4f;
                                    "
                                  >
                                    ${code}
                                  </p>
                                </div>
                              </div>
                        
                              <p
                                style="
                                  max-width: 400px;
                                  margin: 0 auto;
                                  margin-top: 30px;
                                  text-align: center;
                                  font-weight: 500;
                                  color: #8c8c8c;
                                "
                              >
                                Need help? Ask at
                                <a
                                  href="huydang.dev@gmail.com"
                                  style="color: #499fb6; text-decoration: none"
                                  >huydang.dev@gmail.com</a
                                >
                              </p>
                            </main>
                          </body>
                        </html>

`
            };
            await this.GeneralTransporter.sendMail(mailOptions, (error, info): void => {
                if (error) {
                    console.log(error.message);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        } catch (error: any) {
            console.log(error.message);
        }
    }

    public async SendingOTP_ResetPassword(
        {
            email,
            token,
            localhost,
            endpoint,
            name
        }: IResetPassword): Promise<void> {

        try {
            const mailOptions = {
                from: config.EmailUser,
                to: email,
                subject: 'Reset Password',
                html: `
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
                        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>Password Reset</title>
                    </head>
                    
                    <body style="margin: 0; padding: 0; font-family: Arial, sans-serif;">
                    
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td bgcolor="#f9f9f9" style="padding: 40px 30px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" style="padding-bottom: 30px;">
                                                <img src="https://firebasestorage.googleapis.com/v0/b/shop-cart-a2bcc.appspot.com/o/—Pngtree—cartoon%20lock_8954510.png?alt=media&token=f901bce7-a02f-45ff-b622-7199b1f5eb0a" alt="Lock" width="50" style="display: block; width: 120px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <h2 style="color: #161a39; font-size: 24px;">Please reset your password</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 20px; color: #666666; font-size: 16px; line-height: 24px;">
                                              <h2>Hello ${name}</h2>
                                                <p>We have sent you this email in response to your request to reset your password.</p>
                                              <p>Please ignore this email if you did not request a password change.</p>
                                                <p>To reset your password, please follow the link below:</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-top: 20px;">
                                                <a href="${localhost}${endpoint}${token}" style="display: inline-block; padding: 15px 40px; background-color: #161a39; color: #ffffff; font-size: 18px; text-decoration: none; border-radius: 10px;">Reset Password</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 20px; color: #888888; font-size: 16px; line-height: 24px;">
                                                <p style="font-style: italic;">The verification link will be valid for <span style="font-size: 18.5px; color: #161a39" ><strong>24 hours</strong></span> from the time this Email is received</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                    </html>
                `
            };
            await this.GeneralTransporter.sendMail(mailOptions, (error, info): void => {
                if (error) {
                    console.log(error.message);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        } catch (error: any) {
            console.log(error.message);
        }
    }
}


export {EmailSending};