import {ConfigEnv} from "../Config/ConfigEnv";
import nodemailer from 'nodemailer';


class MaillerConfig {
    private config: ConfigEnv;
    protected GeneralTransporter: nodemailer.Transporter;

    constructor() {
        this.config = new ConfigEnv();
        this.GeneralTransporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            auth: {
                user: this.config.EmailUser,
                pass: this.config.EmailPassword,
            }
        });
    }
}

export {MaillerConfig};